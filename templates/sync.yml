spec:
  inputs:
    stage:
      default: sync
      description: "Defines the stage where the dataDir is synced"
    gib_image_base:
      default: "registry.gitlab.com/gitlab-com/gl-security/product-security/gib"
    gib_image_version:
      default: ":latest"
    gib_log_level:
      default: "info"
      description: "Set GIB log level"
      options:
        - panic
        - fatal
        - error
        - warn
        - info
        - debug
        - trace
    dataDir:
      default: "./data"
      description: "Path to dataDir in the repository"
    # We need another directory to copy the dataDir during the sync.
    # It can't be the same as the dataDir above because syncing can delete some folders if groups/projects are deleted or archived.
    # If the same directory is used, the artifact of this job is restored on the original dataDir, and the deleted folders would be preserved.
    working_dataDir:
      default: "./working_data"
      description: "Working dataDir. Must be different from the `dataDir` input."
    gitlab_api_token:
      default: ""
      description: "Access token for the GitLab API"
---
sync:
  stage: $[[ inputs.stage ]]
  image:
    name: $[[ inputs.gib_image_base | expand_vars ]]$[[ inputs.gib_image_version ]]
    entrypoint: [""]
  script:
    - set -x
    - if [ "$DATA_DIR_PATH_IN_REPO" = "$DATA_DIR_PATH" ]; then echo "inputs.working_dataDir must be different from inputs.dataDir"; exit 1; fi
    - mkdir -p $DATA_DIR_PATH
    # We need to keep the files in $DATA_DIR_PATH_IN_REPO that would be cached
    - cp -a $DATA_DIR_PATH_IN_REPO/* $DATA_DIR_PATH
    # Cleanup ignored folders
    - |
      find $DATA_DIR_PATH -name 'ignore' -print0 |
        while IFS= read -r -d '' ignore_path; do
          cp $ignore_path /tmp
          rm -rf $(dirname $ignore_path)/*
          mv /tmp/ignore $ignore_path
        done
    - /usr/local/bin/gib sync
  variables:
    LOGLEVEL: $[[ inputs.gib_log_level ]]
    GITLAB_API_TOKEN: $[[ inputs.gitlab_api_token ]]
    DATA_DIR_PATH_IN_REPO: $[[ inputs.dataDir ]]
    DATA_DIR_PATH: $[[ inputs.working_dataDir ]]
  artifacts:
    paths:
      - $[[ inputs.working_dataDir ]]
  rules:
    - if: '$GITLAB_API_TOKEN == "" && $GITLAB_PASSWORD == ""'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
