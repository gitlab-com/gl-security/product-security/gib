package categories

import (
	"encoding/json"
	"fmt"
	"strings"
)

// Category is the name of a project category
type Category string

// Categories are currently hardcoded
// TODO: Make them configurable via a config file
const (
	// Product is a project part of the GitLab Product
	Product = "product"

	// Library refers to libraries (Ruby Gem for example)
	Library = "library"
	// Website refers to Website (use with `internal`/`external`)
	Website = "website"
	// API refers to APIs (use with `internal`/`external`)
	API = "api"
	// Service refers to projects exposing ports
	Service = "service"
	// Deploy refers to projects used to deploy GitLab (like Helm Charts)
	Deploy = "deploy"

	// Projects used to deploy on runway (https://docs.runway.gitlab.com/)
	Runway = "runway"

	// Internal refers to user-facing applications, internal only
	Internal = "internal"
	// External refers to user-facing applications, open to the WWW
	External = "external"

	// Data classification
	// https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html

	// GreenData refers to Green Data
	GreenData = "green_data"
	// YellowData refers to Yellow Data
	YellowData = "yellow_data"
	// OrangeData refers to Orange Data
	OrangeData = "orange_data"
	// RedData refers to Red Data
	RedData = "red_data"

	// 3rdParty refers to projects integrating with 3rd parties
	ThirdParty = "3rd_party"

	// POCs, tests, and demos, nothing to see here.

	// Demo is for demos
	Demo = "demo"
	// Test is for test projects
	Test = "test"
	// POC is for POCs
	POC = "poc"

	// Team is used for team work (tasks, organization, ...). No code
	Team = "team"

	// Temporary is for temporary projects, should be `marked_for_deletion` after a certain time
	Temporary = "temporary"

	// UsePAT is for projects using Personal (or Project) Access Token
	UsePAT = "use_pat"

	// Deprecated is for deprecated projects (but should keep)
	Deprecated = "deprecated"

	// MarkedForDeletion is for projects we should remove altogether
	MarkedForDeletion = "marked_for_deletion"

	// KeepPrivate is for projects we should keep private indefinitely
	KeepPrivate = "keep_private"

	// Docs is for projects generating documentation
	Docs = "docs"

	// Tooling is for tooling projects
	Tooling = "tooling"

	// Containers is for projects building Docker images
	Container = "container"

	// Fork is for forks of projects.
	Fork = "fork"

	// SecretsMonitoring is for projects monitoring secrets
	SecretsMonitoring = "secrets_monitoring"

	// SecurityPolicyProject is a project to manage security policies. See https://docs.gitlab.com/ee/user/application_security/policies/
	SecurityPolicyProject = "security_policy_project"
)

// Parse a string category and returns the corresponding Category, or "".
func Parse(cat string) Category {
	if strings.HasPrefix(cat, "system::") && cat != "system::" {
		return Category(cat)
	}

	if strings.HasPrefix(cat, "arch:") && cat != "arch:" {
		return Category(cat)
	}

	switch cat {
	case "product", "library", "website", "api", "service", "deploy", "internal", "external", "3rd_party", "team", "temporary", "deprecated", "use_pat", "marked_for_deletion", "keep_private", "docs", "tooling", "container", "fork", "secrets_monitoring", "security_policy_project":
		return Category(cat)

	case "green_data", "yellow_data", "orange_data", "red_data":
		return Category(cat)

	case "demo", "test", "poc":
		return Category(cat)
	}

	return ""
}

// UnmarshalJSON parses the category as a string
func (c *Category) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	*c = Parse(s)
	if *c == Category("") {
		return fmt.Errorf("unknown category: %q", s)
	}
	return nil
}
