**On this page:**

[[_TOC_]]

# GitLab Inventory Builder (GIB)

The GitLab Inventory Build (`gib`) is a tool to generate and maintain a complete inventory of
projects (hosted on GitLab.com or Self-Managed) with their dependencies.

The project was built with the following constraints in mind:

- Easy and lightweight usage (no daemon)
- No dependency on external services, other than GitLab itself
- Easy to query data
- Flexible reports generation
- Can work with gitlab.com or self-hosted instances

## Product Dogfooding

One of the goals of GitLab Inventory Builder is to incubate features and automation, which may be iterated upon to build GitLab product features.

The following epics and issues track plans to build or iterate on GitLab product features, based on or similar to GIB features and use cases:

1. https://gitlab.com/groups/gitlab-org/-/epics/8226 - Tracks plans to allow users to search, filter, and group the GitLab [Dependency List](https://docs.gitlab.com/ee/user/application_security/dependency_list/) at the group and subgroup levels so users can easily search across all their projects to find vulnerable dependencies.

**Completed**

The following epics track features implemented or improved in GitLab, which replace or deprecate GIB features or use cases:

1. https://gitlab.com/groups/gitlab-org/-/epics/7622 - Tracks plans to make GitLab [security approval policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html) available at the group and subgroup levels so the policies can automatically apply to newly created projects to require approval when a merge request contains newly detected vulnerabilities that meet the policy criteria. This was [released in GitLab 15.6](https://about.gitlab.com/releases/2022/11/22/gitlab-15-6-released/#group-and-subgroup-level-scan-result-policies).
1. https://gitlab.com/groups/gitlab-org/-/epics/8092 - Tracks plans to introduce license approval policies in GitLab so that users can enforce policies that will automatically apply to newly created projects to require approval when a merge request introduces a new dependency or a dependency with a denied license.  This was [released in GitLab 15.9](https://about.gitlab.com/releases/2023/02/22/gitlab-15-9-released/#manage-license-approval-policies).
1. https://gitlab.com/groups/gitlab-org/-/epics/5510 - Tracks plans to allow security policies to only apply to projects with a specified [Compliance Framework](https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-frameworks) label.  This was [released in GitLab 16.11](https://about.gitlab.com/releases/2024/04/18/gitlab-16-11-released/#security-policy-scopes).

## Usage

To build an inventory, create a new empty project, and add a `.gitlab-ci.yml` containing:

```yaml
include:
  remote: 'https://gitlab.com/gitlab-com/gl-security/product-security/gib/-/raw/main/ci/Inventory-Builder.gitlab-ci.yml'
```

This GitLab-CI template defines variables that you can
[override](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence) in your project if
needed.

The next step is to [configure](#configuration) your project, and run a pipeline.  `gib` will sync
your data and create merge requests to let you review the changes.

Reports are also generated and available as pipeline artifacts.

It is recommended to run [scheduled](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
pipelines (for example once a week) to keep the inventory in sync automatically.

### Configuration

The `data` folder (default for `$DATA_DIR_PATH`) is used as configuration (see [the folders section
below](#folders)). To scan groups, add them at the root of this folder.

```
.
└── data
    ├── gitlab-com
    └── gitlab-org
        ├── memory-team
        │   └── ignore
        └── security
            └── ignore
```

This layout will sync the projects and subgroups under `gitlab-com` and `gitlab-org`.
The ones under `gitlab-org/memory-team` and `gitlab-org/security` will be ignored.

`gib` will save a `group.json` data file in every subgroup folder being synced. This file is created
from the upper group [subgroups
data](https://docs.gitlab.com/ee/api/groups.html#list-a-groups-subgroups). The same goes for their
[projects](https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects), with a `project.json`
file for each project.

#### Categorization of projects

Projects needs to be categorized to have policies applied. This operation is done by adding
`properties.yml` files to the inventory repository, in the data folder. Only
projects can be categorized (see `categories/categories.go` for the list of available categories).
To categorize a project, add a `categories` array to the `properties.yml` file of a project. For
example, if the content of `data/gitlab-org/gitlab-runner/properties.yml` is:

```yaml
categories:
  - product
  - container
```

Then the project `gitlab-org/gitlab-runner` is categorized as `product` and `container`. This
operation should be done every time the `sync` scheduled CI/CD job is running and a Merge Request is
created by the `commit-changes` job.
To help categorizing new projects, the following shell function can be used:

```sh
# Set categories in the inventory
# usage: set_cat <filename> <categories...>
function categorize() {
  local file=${1/project.json/properties.yml}
  if [[ "$#" -eq 1 ]]; then
    cat $file
    return
  fi
  shift
  local arr=("$@")
  local categories=$(echo \""${${arr[@]}//${IFS:0:1}/\", \"}\"")
  touch $file
  yq -i ".categories= [$categories]" $file
  gib validate -q $(dirname $file)
}
```

Secrets can (and should) be stored in [1Password](https://1password.com/). To use gib with the
1Password cli (`op`), you can alias `gib` to:

```sh
$ alias gib='op run --env-file=/path_to_your_env_file.env -- gib -c'
```

#### Software Systems

Software Systems are a special kind of categories that are more flexible and meant to group projects
together by the GitLab software system they implement. Unlike categories, these are not hard-coded
values.

Software Systems are part of the `categories` array in projects properties (`properties.yml`) and
are similar to GitLab scoped labels: they start with `system::` and end with the name of the
software system. For example, Gitlab Runner projects can be grouped like this:

```yaml
categories:
  - system::gitlab-runner
  - product
```

Software Systems are typically used to get groups of projects when generating SBOMs.

#### Architectures

Architectures are also a special kind of categories meant to group projects together by the software
system architecture they implement. Like Software Systems, these are not hard-coded values.

Architectures are part of the `categories` array in projects properties (`properties.yml`). They
start with `arch:` and end with the name of the architecture. For example, the Omnibus Reference
Architecture 1k projects can be grouped like this:

```yaml
categories:
  - system::gitlab-core
  - arch:omnibus_ra1k
  - product
```

Architectures are typically used along with a [Software System](#software-system) to get groups of
projects when generating SBOMs.

#### Directory traversal

In the following example, the subgroups and projects or `gitlab-org` won't be synced, but since we
created the `shared-runners` folder, this one will be synced.

```
.
└── data
    └── gitlab-org
        ├── ignore
        └── shared-runners
            ├── images
            ├── homebrew
            └── macos
```

When running `gib sync` on this data folder, the result will be:

```
.
  └── data
      └── gitlab-org
        ├── ignore
        └── shared-runners
            ├── images
            │   └── project.json
            ├── gcp
            │   └── group.json
            │   └── windows-containers
            │       └── project.json
            ├── macstadium
            │   └── group.json
            │   └── orka
            │       └── project.json
            ├── homebrew
            │   └── project.json
            └── macos
                └── project.json
```

(based on current data as we're writing this documentation).

#### Standalone projects

In the following example, the subgroups and projects or `gitlab-org` won't be synced, but since we
created the `ci-cd/docker-machine` folder with a `properties.yml` file, `gib` knows it's a project.

```
.
└── data
    └── gitlab-org
        ├── ignore
        └── ci-cd
            ├── ignore
            └── docker-machine
                └── properties.yml
```

When running `gib sync` on this data folder, the result will be:

```
.
└── data
    └── gitlab-org
        ├── ignore
        └── ci-cd
            ├── ignore
            └── docker-machine
                ├── project.json
                └── properties.yml
```

Only the `docker-machine` project has been synced. This layout is useful when you don't want to sync
all the projects under a group, but it comes at a price: these projects are synced one by one,
instead of batches (of 100) when in a subgroup.

#### GitLab API Permissions

In order to sync your inventory, `gib` needs an authorized
access to the GitLab API. A [Personal Access
Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) or [Group Access
Token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) with the `read_api`
and `read_repository` scopes can be used by setting the `$GITLAB_API_TOKEN` variable.

This `--api-token` parameter is optional if all top-level namespaces have [local
tokens](#use-multiple-sync-tokens) configured in the inventory.

When a sync occurs as part of a scheduled pipeline, gib creates a Merge Request to let you review
changes in the projects and namespaces monitored in the inventory. This action requires a token with
the right privileges: a [project access
token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with the `api`
scope is the best fit for this. Check out the `.gitlab-ci.yml` file of the
[inventory-example](https://gitlab.com/gitlab-com/gl-security/product-security/inventory-example)
project to see how to use this token in your inventory.

### Sync projects data

`gib sync` will sync the `$DATA_DIR_PATH` by connecting to the GitLab API (`$GITLAB_API_BASEURL`).

```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib sync
```

#### Sync a sub-tree

To update a portion of `$DATA_DIR_PATH` only, the first argument passed to `gib sync` lets you specify
a folder under `$DATA_DIR_PATH`. The folder can be a group or a single project, for example:

```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib sync ./data/gitlab-org/gitlab-services/design.gitlab.com
```

will sync only the project `gitlab-org/gitlab-services/design.gitlab.com`.

#### Use multiple sync tokens

By default, gib will sync the inventory with a single `GITLAB_API_TOKEN`. This token can be
overridden at any level of the `data` folder. When a file `sync_token_name` is created in a folder,
gib will sync this folder (group or project) with the token specified in this file.

The content of the file is **never** the token itself, but the **name** of the environment variable
to use in lieu of `GITLAB_API_TOKEN`. **As a reminder: never commit secrets in the inventory
directly**.

Example:

```
.
└── data
    ├── gitlab-com
        ├── sync_token_name
        └── www-gitlab-com
    └── gitlab-org
        └── gitlab-runner
```


If `data/gitlab-com/sync_token_name` contains `GITLABCOM_SYNC_TOKEN`, then the **value** of
`GITLABCOM_SYNC_TOKEN` will be used to sync `data/gitlab-com` and all its sub groups and projects.
`data/gitlab-org` will be synced with `GITLAB_API_TOKEN` as usual.

When syncing a folder, gib will search for a `sync_token_name` file in the current directory and all
the parent directories until this file is found or the root of the data directory is reached. It
means that running the following command will sync `data/gitlab-com/www-gitlab-com` with the
`GITLABCOM_SYNC_TOKEN` because `data/www-gitlab-com` contains a `sync_token_name` file:

```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib sync data/gitlab-com/www-gitlab-com
```

### Validate projects data

While the `$DATA_DIR_PATH` is being synced automatically by `gib`, its configuration is mostly done by
users, and therefore it must be validated. The command can be used prior to committing files like
`properties.yml` to your inventory repository.


```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib validate
```

The command validates the name of files in `$DATA_DIR_PATH`, among files [used by gib](#files_used_by_gib)
and [permitted](#permitted_files). The content of the [used by gib](#files_used_by_gib) is also
validated.

### Generate HTML reports

Gib can generate HTML reports with the data of your inventory. These reports provide useful metrics
on vulnerabilities (for `product` projects).


```sh
$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib generate-reports
```

The `GITLAB_API_TOKEN` is required to fetch user data for the reports. Once the command executed,
the reports are available in the `public/` folder of your inventory.

### Self-hosted GitLab

By default, `gib` will use the gitlab.com API located at https://gitlab.com/api/v4. `gib` can be
used with a different API URL by setting the `$GITLAB_API_BASEURL` variable. If several hosts must
be used, run `gib` multiple times with different `data` folders and API URLs.

### Compliance

The [CI/CD template](ci/Inventory-Builder.gitlab-ci.yml) has a `compliance` stage, along with an
`opa` job. This job evaluates projects and sites for violations (see [this Merge
Request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/83315/) for details on
GitLab's default polices).

If the local inventory contains the following directories:
- `polices/projects/`
- `policies/sites/`
- `policies/vulnerabilities/`

they will be used in lieu of the [default ones](policies/) shipping with Gib. Don't forget to set
the right variables for the queries as well when using the CI/CD template.

To find violations, run:

```sh
$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib compliance run
```

#### Create issues

By default, `gib` will create issues for the violations found during the `compliance` stage. One
issue is created by violation. Scoped labels with the project ID and a key for the violation are
applied to the issue. To execute this feature locally, you need to run 2 commands, the first is
needed to export the violations to a temporary json file.

```sh
$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib compliance export
$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  -e GITLAB_API_TOKEN \
  -e PROJECT_ID=123 \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib compliance create-issues
```

When violations are not reported anymore, the corresponding issue will be labeled as `~resolved`. It
is the responsibility of the user to close the issue after verification. If the violation is
detected again, the label is removed, and the violation marked as `Reopened`.

Closed issues for reported violations will be reopen automatically. The corresponding violation will
have a `Reopened` badged next to them in the reports. To keep them closed see the next section to
ignore some violations.

#### Ignoring some violations

Not all violations are relevant, and it's sometimes useful to ignore them to limit the noise. To
ignore a violation, apply the `~ignore` label to the corresponding issue. The issue can then be
closed, and will stay closed. The violation will still appear in the reports, with an `Ignored`
badge next to them.

#### Allow users to push to the default branch

To create an allow list of users who are permitted to push to the default branch, update the
`properties.yml` file of the project with a `protected_branches_allow_list` array.

For example, to allow (not report a violation) `Maintainers` to push to the default branch:


```yaml
categories:
  - product

protected_branches_allow_list:
  - name: main
    push_access_levels:
      - access_level: 40
        access_level_description: "Maintainers"
        user_id: null
        group_id: null

```

The array will be compared with the one of the default branch returned by the GitLab API without the
`id` of the push_access_level. The branch `name` is necessary, as GIB might validate all protected
branches in the future.

### Using the generated SQLite database

All data fetched is stored in the local SQLite database generated by the `update-db` CI/CD job.
This database is located in the `db/` folder and is named `inventory.db`. Only a compressed version
of this database is stored in git (using `git-lfs`).

To use this database, it needs to be decompressed first:

- Download the compressed DB `db/inventory.db.7z` from your inventory repo
- Extract the database with [7z](https://www.7-zip.org/download.html): `7z x db/inventory.db.7z`
- Check that `db/inventory.db` is present

Examples queries below use the `-markdown` flag to output in this format, but `-json` can be used
too. See the SQLite documentation for help on output formats.

#### Get Product Projects

To extract all projects having the `product` category, run the following query on the database:

```sh
$ sqlite3 -markdown db/inventory.db "SELECT * FROM product_projects;"
```

This query is using the `product_projects` view. To see the definition of this view, use the command
`.schema product_projects` in SQLite.

#### Search for dependencies

Run this query on the database:

```sh
$ cat <<EOF | sqlite3 -markdown db/inventory.db
SELECT d.name, d.version, p.path_with_namespace
FROM dependencies as d
JOIN projects p ON d.project_id = p.id
WHERE LOWER(d.name) LIKE 'some_package';
EOF
```

To search for dependencies having a specific license:

```sh
cat <<EOF | sqlite3 -markdown db/inventory.db
SELECT d.name, d.version, p.path_with_namespace
FROM dependencies AS d
JOIN projects p ON d.project_id = p.id
WHERE exists (SELECT 1 FROM json_each(licenses) WHERE value = 'License name');
EOF
```

To fetch licenses with their attributes (only URL available for now):

```sh
cat <<EOF | sqlite3 -markdown db/inventory.db
SELECT d.name, d.version, p.path_with_namespace, licenses.*
FROM (SELECT json_group_array(json_object('name', name, 'url', url)) FROM licenses) AS licenses
JOIN dependencies as d
JOIN projects p ON d.project_id = p.id
WHERE EXISTS (SELECT 1 FROM json_each(licenses) WHERE value = 'License name');
EOF
```
#### A link to the past

The inventory database can also be used to go "back in time" using the git history of your inventory
repository. Using git, a previous version of the database can be restored and used along with
most recent version of the database.

For example, to find the projects where a setting switched since the version you restored:

```sh
# This is the version of the database from one week ago:
$ ls inventory_past.db
# Open the latest version of the database in the repository:
$ sqlite3 inventory.db
```

```sql
attach 'inventory_past.db' as 'past';
select main.projects.id,
  main.projects.path_with_namespace,
  main.projects.some_setting_enabled,
  past.projects.some_setting_enabled some_setting_enabled_in_past
from main.projects join past.projects on main.projects.id = past.projects.id
where main.projects.some_setting_enabled <>  past.projects.some_setting_enabled
order by main.projects.path_with_namespace;
```

In this example, the database opened from `inventory.db` is available under the `main` schema name,
and `inventory_past.db` is attached as the `past` namespace.

#### Limitations

SQLite doesn't support [boolean values](https://www.sqlite.org/datatype3.html#boolean_datatype), so
`true` and `false` values are converted to `1` and `0` (integers).

### Manually add dependencies

Dependencies are fetched from the [GitLab API](https://docs.gitlab.com/ee/api/dependencies.html),
which is relying on [Dependency Scanning reports]. The list of dependencies in the inventory can be
augmented with the `properties.yml` file, and the `dependencies` keyword. `dependencies` is an array
of [`Dependency`](dependencies.go).

[Dependency Scanning reports]: https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdependency_scanning

Example:

```yaml
categories:
  - product

dependencies:
  - name: "bitnami/jruby"
    version: "latest"
    package_manager: "docker"
    dependency_file_path: "Dockerfile:18"
    path_with_namespace: "gitlab-org/containers/bitnami/jruby"
```

`package_manager` is not enforcing specific values, and can be set to anything.
`path_with_namespace` (optional) is a key specific to this inventory. It links the dependency with a project of
the inventory. If set, it must be the `path_with_namespace` of the dependency project to allow
cross-linking.

#### Extract version

Because declared dependencies become static when being maintained manually, it can be tedious and
error prone to store their versions in the `dependencies` array. Therefore, it's often better to use
a `version_extract` field instead. When set, this map will trigger a fetch of the version
directly from a file in the repository of the project.

Example:

```yaml
categories:
  - product

dependencies:
  - name: "bitnami/jruby"
    version_extract:
      file_path: Dockerfile
      regexp: '(?m)^\s+image: "bitnami\/jruby:(.*)"'
      ref: master
    package_manager: "docker"
    dependency_file_path: "Dockerfile:18"
    path_with_namespace: "gitlab-org/containers/bitnami/jruby"
```

In this example, the `version` of "bitnami/jruby" will be set during the [`sync`
phase](#sync_projects_data) with the value extracted thanks to the `regexp` and the content of
the file `Dockerfile`.

Note: `ref` is optional, and will default to the default branch of the repository if omitted.

### Folders

Gib is using these folders:

- `data`: contains all the projects metadata
- `db`: computed metadata in a single SQLite3 file
- `reports`: Generated reports

#### Files used by gib

The following files are used by `gib`:
- `ignore`: Skip the sync of a group or a project
- `properties.yml`: Used for categorization of projects
- `project.json`: Stored projects metadata (generated)
- `group.json`: Stored groups metadata (generated)
- `dependencies.json`: Stored projects dependencies (generated)
- `vulnerabilities.json`: Stored projects vulnerabilities (generated)
- `ci-config.json`: Stored projects ci-configuration (generated)
- `protected_branches.json`: Stored projects protected branches (generated)
- `images-requested.json`: Inventory of Docker images used in a project (generated)
- `job_token_scope_allowlist.json`: Project Job Token allow list (generated)

#### Permitted files

- `README.md`
- `.gitkeep`
- `.DS_Store`

Any other file will be considered invalid. Feel free to contribute to this project if other files
need to be added.

### Inventory Docker images

Gib has a built-in command to inventory Docker images requested in GitLab CI/CD Jobs:

```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib images requested
```

This command walks the `./data` directory and for each project fetches the last jobs and their
traces (raw log output) to look for a pattern corresponding to the GitLab Runner using a Docker
image for the current job.

The command is incremental. If `./data` is cached, the next run will stop downloading jobs for a
project once it has reached the last one parsed. This means traces are downloaded only once.

Jobs are kept in the cache for 3 months: This duration can't be modified at this time.

Similarly to other commands, a sub path of `./data` can be used to fetch images only under this
group (recursively) or project:

```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/product-security/gib images requested data/gitlab-org/gitlab-runner
```

## Development

This project is organized in different parts:

- a [CI/CD template](ci/Inventory-Builder.gitlab-ci.yml) with the complete workflow.
- Go files to build the `gib` binary. This binary is the sole entrypoint of the [docker
  image](Dockerfile) generated in this project.
- [Script files](scripts/) as a shortcut for complex operations. They are
  [embedded](https://pkg.go.dev/embed) into the `gib` binary to keep a single entrypoint.
- [Policies](policies/) files written in Rego (the Open Policy Agent language).
- A [Hugo](https://gohugo.io) [theme](reports/theme/) to generate the HTML reports.

### Go development guidelines

- Install [pre-commit](https://pre-commit.com/) to validate your changes before committing.
- It is recommended to install [GoConvey](https://github.com/smartystreets/goconvey) to run the
  test suite every time a file is changed (this should also open a browser window on
  http://127.0.0.1:8080/):

```sh
$ go install github.com/smartystreets/goconvey@latest
$ goconvey -excludedDirs testdata,ci,scripts,reports,policies
```

### Build the docker image

On Silicon Macs (MacBook Pro M1 and later):

```sh
$ docker build -t gib --build-arg ARCH=arm64 .
```

On other Intel-based computers, simply run:

```sh
$ docker build -t gib .
```

### Edit the Hugo theme

An inventory site needs to be created to generate HTML files with [Hugo](https://gohugo.io).
The folder `inventory_test/` is used for this purpose.

Running the script `scripts/dev.sh` (requires the `gib` docker image created
[above](#build_the_docker_image)) will generate a new static site and runs Hugo to serve it on
http:localhost:1313.

You can add more projects in `inventory_test/data/` by creating a new folder, for example
`inventory_test/data/gitlab-com` to keep `inventory_test/data/gitlab-org` clean for tests.
