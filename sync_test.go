package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"testing"

	"github.com/google/go-cmp/cmp"
	log "github.com/sirupsen/logrus"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

func TestSyncCmd(t *testing.T) {
	mux, ts := testServer(t)
	defer ts.Close()

	// Create temp data-dir
	dataDir, err := os.MkdirTemp("", "datadir")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dataDir)

	// Configure dataDir with a main `gitlab-org` group, and an existing project
	err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "advisories-community"), 0o700)
	if err != nil {
		t.Fatal(err)
	}

	os.Setenv("GITLAB_API_TOKEN", "asdf123")
	os.Setenv("GITLAB_API_BASEURL", ts.URL)
	os.Setenv("DATA_DIR_PATH", dataDir)

	log.SetFormatter(&log.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	output := new(bytes.Buffer)
	app := NewApp()
	app.Writer = output

	t.Run("Correct output", func(t *testing.T) {
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Project folders and metadata stored", func(t *testing.T) {
		projectsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "projects.json"))
		if err != nil {
			t.Fatal(err)
		}

		var projects []*gitlab.Project
		err = json.Unmarshal(projectsJSON, &projects)
		if err != nil {
			t.Fatal(err)
		}
		for _, p := range projects {
			want, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", p.Path, "project-short.json"))
			if err != nil {
				t.Fatal(err)
			}

			got, err := os.ReadFile(filepath.Join(dataDir, "gitlab-org", p.Path, "project-short.json"))
			if err != nil {
				orgErr := err
				files, err := os.ReadDir(filepath.Join(dataDir, "gitlab-org", p.Path))
				if err != nil {
					log.Fatal(err)
				}

				for _, file := range files {
					fmt.Println(file.Name())
				}
				t.Fatal(orgErr)
			}

			if diff := cmp.Diff(want, got); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}
		}
	})

	t.Run("SubGroups", func(t *testing.T) {
		t.Run("Folders created", func(t *testing.T) {
			subgroupsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "subgroups.json"))
			if err != nil {
				t.Fatal(err)
			}

			var groups []*gitlab.Group
			err = json.Unmarshal(subgroupsJSON, &groups)
			if err != nil {
				t.Fatal(err)
			}

			for _, g := range groups {
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", g.Path)); os.IsNotExist(err) {
					t.Errorf("Group %q folder was not created", g.Path)
				}
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", g.Path, "group.json")); os.IsNotExist(err) {
					t.Errorf("Group %q metadata was not created", g.Path)
				}
			}
		})
		t.Run("Projects folders created", func(t *testing.T) {
			fiveMinAppProjectsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "5-minute-production-app", "projects.json"))
			if err != nil {
				t.Fatal(err)
			}
			var fiveMinAppProjects []*gitlab.Project
			err = json.Unmarshal(fiveMinAppProjectsJSON, &fiveMinAppProjects)
			if err != nil {
				t.Fatal(err)
			}
			for _, p := range fiveMinAppProjects {
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "5-minute-production-app", p.Path)); os.IsNotExist(err) {
					t.Errorf("Project %q folder was not created", p.Path)
				}
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "5-minute-production-app", p.Path, "project.json")); os.IsNotExist(err) {
					t.Errorf("Project %q metadata was not created", p.Path)
				}
			}
		})
	})

	t.Run("Ignore directories with 'ignore' files", func(t *testing.T) {
		err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "security"), 0o700)
		if err != nil {
			t.Fatal(err)
		}
		// Ignore the group "gitlab-org/security"
		// Since the http test server doesn't know this route, this test will fail if we try to get the projects of this group
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "security", "ignore"), []byte{}, 0o600)
		if err != nil {
			t.Fatal(err)
		}
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Traverse directories with 'ignore' files", func(t *testing.T) {
		// In this test, gitlab-org/ci-cd contains an `ignore` file, but also a subdirectory `shared-runners`.
		// gitlab-org/ci-cd/shared-runners should be synced, but not gitlab-org/ci-cd

		CICDSharedRunnersProjects, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "ci-cd", "shared-runners_projects.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/groups/gitlab-org%2Fci-cd%2Fshared-runners/projects", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(CICDSharedRunnersProjects))
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/groups/gitlab-org%2Fci-cd%2Fshared-runners/subgroups", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "[]")
		}).Methods(http.MethodGet)

		// Ignore the group "gitlab-org/ci-cd"
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ci-cd", IgnoreFile), []byte{}, 0o600)
		if err != nil {
			t.Fatal(err)
		}

		// gitlab-org/ci-cd is ignored, but traversed. Therefore this directory will be synced:
		err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners"), 0o700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners"))

		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		t.Run("Projects folders created", func(t *testing.T) {
			var sharedRunnerProjects []*gitlab.Project
			err = json.Unmarshal(CICDSharedRunnersProjects, &sharedRunnerProjects)
			if err != nil {
				t.Fatal(err)
			}

			for _, p := range sharedRunnerProjects {
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners", p.Path)); os.IsNotExist(err) {
					t.Errorf("Project %q folder was not created", p.Path)
				}
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners", p.Path, "project.json")); os.IsNotExist(err) {
					t.Errorf("Project %q metadata was not created", p.Path)
				}
			}
		})
	})

	t.Run("Sync standalone projects", func(t *testing.T) {
		// In this test, gitlab-org/ci-cd contains an `ignore` file, but also a subdirectory `docker-machine`, in turn containing a `properties.yml` files.
		// gitlab-org/ci-cd/docker-machine is a "standalone project", and should be synced, but not gitlab-org/ci-cd.
		dmProjectJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "ci-cd", "docker-machine.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fci-cd%2Fdocker-machine", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(dmProjectJSON))
		}).Methods(http.MethodGet)

		// Ignore the group "gitlab-org/ci-cd"
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ci-cd", IgnoreFile), []byte{}, 0o600)
		if err != nil {
			t.Fatal(err)
		}

		dmPath := filepath.Join(dataDir, "gitlab-org", "ci-cd", "docker-machine")
		// gitlab-org/ci-cd is ignored, but traversed. Therefore this project will be synced:
		err = os.MkdirAll(dmPath, 0o700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(dmPath)

		err = os.WriteFile(filepath.Join(dmPath, PropertiesFile), []byte("categories:"), 0o600)
		if err != nil {
			t.Fatal(err)
		}

		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		t.Run("have projects.json", func(t *testing.T) {
			if _, err := os.Stat(filepath.Join(dmPath, "project.json")); os.IsNotExist(err) {
				t.Error("Project \"docker-machine\" was not synced")
			}
		})
	})

	t.Run("Ignore archived projects", func(t *testing.T) {
		archievedProject, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "archived.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/groups/with-archived/projects", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(fmt.Sprintf("[%s]", archievedProject)))
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/groups/with-archived/subgroups", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "[]")
		}).Methods(http.MethodGet)

		err = os.Mkdir(filepath.Join(dataDir, "with-archived"), 0o700)
		if err != nil {
			t.Fatal(err)
		}
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		files, err := os.ReadDir(filepath.Join(dataDir, "with-archived"))
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			if f.IsDir() && f.Name() == "gitlab-development-kit" {
				t.Error("Project 'gitlab-development-kit' is archived and should not be synced")
			}
		}

		t.Run("Clean-up archived projects", func(t *testing.T) {
			err = os.Mkdir(filepath.Join(dataDir, "with-archived", "gitlab-development-kit"), 0o700)
			if err != nil {
				t.Fatal(err)
			}
			err = os.WriteFile(filepath.Join(dataDir, "with-archived", "gitlab-development-kit", ProjectMetadataFile), []byte("{}"), 0o600)
			if err != nil {
				t.Fatal(err)
			}
			err = app.Run([]string{os.Args[0], "sync"})
			if err != nil {
				t.Fatal(err)
			}

			files, err := os.ReadDir(filepath.Join(dataDir, "with-archived"))
			if err != nil {
				log.Fatal(err)
			}

			for _, f := range files {
				if f.IsDir() && f.Name() == "gitlab-development-kit" {
					t.Error("Project 'gitlab-development-kit' is archived and should have been removed")
				}
			}
		})
	})

	t.Run("Clean-up projects", func(t *testing.T) {
		err = os.Mkdir(filepath.Join(dataDir, "gitlab-org", "old-project"), 0o700)
		if err != nil {
			t.Fatal(err)
		}
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "old-project", ProjectMetadataFile), []byte("{}"), 0o600)
		if err != nil {
			t.Fatal(err)
		}
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		files, err := os.ReadDir(filepath.Join(dataDir, "gitlab-org"))
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			if f.IsDir() && f.Name() == "old-project" {
				t.Error("Project 'old-project' should have been deleted")
			}
		}
	})

	t.Run("Clean-up groups", func(t *testing.T) {
		err = os.Mkdir(filepath.Join(dataDir, "gitlab-org", "old-group"), 0o700)
		if err != nil {
			t.Fatal(err)
		}
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "old-group", GroupMetadataFile), []byte("{}"), 0o600)
		if err != nil {
			t.Fatal(err)
		}

		err = os.Mkdir(filepath.Join(dataDir, "gitlab-org", "ignored-group"), 0o700)
		if err != nil {
			t.Fatal(err)
		}
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ignored-group", IgnoreFile), []byte("{}"), 0o600)
		if err != nil {
			t.Fatal(err)
		}

		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ignored-group", ".DS_Store"), []byte("{}"), 0o600)
		if err != nil {
			t.Fatal(err)
		}

		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		files, err := os.ReadDir(filepath.Join(dataDir, "gitlab-org"))
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			if f.IsDir() {
				if f.Name() == "old-group" {
					t.Error("Group 'old-group' should have been deleted")
				}
				if f.Name() == "ignored-group" {
					t.Error("Group 'ignored-group' should have been deleted")
				}
			}
		}
	})

	t.Run("Product projects", func(t *testing.T) {
		propertiesYAML := `categories:
  - product
  - api
  - internal
  - red_data
`

		declarativePolicyPath := filepath.Join(dataDir, "gitlab-org", "declarative-policy")
		PropertiesFile := filepath.Join(declarativePolicyPath, "properties.yml")

		err = os.WriteFile(PropertiesFile, []byte(propertiesYAML), 0o600)
		if err != nil {
			t.Fatal(err)
		}

		tt := []struct {
			Name string
			File string
		}{
			{"Dependency reports", "dependencies.json"},
			{"Vulnerability reports", "vulnerabilities.json"},
			{"CI-configuration reports", "ci-config.json"},
			{"Protected branches", "protected_branches.json"},
			{"Approval Config", "approvals.json"},
			{"Approval Rules", "approval_rules.json"},
			{"CI/CD Settings", "cicd_settings.json"},
			{"JobTokenInboundAllowList", "job_token_scope_allowlist.json"},
		}

		for _, test := range tt {
			t.Run("should store "+test.Name, func(t *testing.T) {
				err = app.Run([]string{os.Args[0], "sync"})
				if err != nil {
					t.Fatal(err)
				}

				want, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", test.File))
				if err != nil {
					t.Fatal(err)
				}

				got, err := os.ReadFile(filepath.Join(declarativePolicyPath, test.File))
				if err != nil {
					if os.IsNotExist(err) {
						t.Errorf("%q was not created", filepath.Join(declarativePolicyPath, test.File))
						return
					}
					t.Fatal(err)
				}

				if diff := cmp.Diff(want, got); diff != "" {
					t.Errorf("Output mismatch (-want +got):\n%s", diff)
				}
			})
		}
	})

	t.Run("Sync sub directory", func(t *testing.T) {
		// In this test, gitlab-org/gitlab-services contains a subdirectory `design.gitlab.com`, in turn containing a `properties.yml` files.
		// Because we use `sync` with an argument, gitlab-org/gitlab-services should not be synced (will result in 404 otherwise)
		projectJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "gitlab-services", "design.gitlab.com.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fgitlab-services%2Fdesign%2Egitlab%2Ecom", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(projectJSON))
		}).Methods(http.MethodGet)

		dPath := filepath.Join(dataDir, "gitlab-org", "gitlab-services", "design.gitlab.com")
		err = os.MkdirAll(dPath, 0o700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(filepath.Join(dataDir, "gitlab-org", "gitlab-services"))

		err = os.WriteFile(filepath.Join(dPath, PropertiesFile), []byte("categories:"), 0o600)
		if err != nil {
			t.Fatal(err)
		}

		err = app.Run([]string{os.Args[0], "sync", filepath.Join(dataDir, "./gitlab-org/gitlab-services/design.gitlab.com")})
		if err != nil {
			t.Fatal(err)
		}

		t.Run("have projects.json", func(t *testing.T) {
			if _, err := os.Stat(filepath.Join(dPath, "project.json")); os.IsNotExist(err) {
				t.Error("Project \"design.gitlab.com\" was not synced")
			}
		})

		t.Run("fails if directory is not a subtree of DATADIR", func(t *testing.T) {
			err = app.Run([]string{os.Args[0], "sync", filepath.Join("/tmp", "./gitlab-org/gitlab-services/design.gitlab.com")})
			if err == nil {
				t.Error("sync with arg `/tmp/[...]` should return an error")
			}
		})
	})

	t.Run("With local Access Token", func(t *testing.T) {
		// Create a new root namespace with a local sync token
		mux.HandleFunc("/api/v4/groups/local-group/subgroups", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(`[{"path": "gitlab-local-token"},{"path": "zzz"}]`))
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/groups/local-group/projects", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string("[]"))
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/groups/local-group%2F{subgroup:(?:gitlab-local-token|zzz)}/{subgroups|projects}", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string("[]"))
		}).Methods(http.MethodGet)

		groupPath := filepath.Join(dataDir, "local-group/gitlab-local-token")
		err = os.MkdirAll(groupPath, 0o700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(groupPath)

		err = os.WriteFile(filepath.Join(groupPath, "sync_token_name"), []byte("local_token"), 0o600)
		if err != nil {
			t.Fatal(err)
		}

		// Since the filepath.Walkdir function used in syncCmd is processing files
		// and folders in lexical order (https://pkg.go.dev/path/filepath#Walk),
		// this namespace should be synced with the main sync token, and not the local one.
		// If the local one is used,
		groupPath2 := filepath.Join(dataDir, "local-group/zzz")
		err = os.MkdirAll(groupPath2, 0o700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(groupPath2)

		os.Setenv("local_token", "321fdsa")

		err = app.Run([]string{os.Args[0], "sync", filepath.Join(dataDir, "local-group")})
		if err != nil {
			t.Fatal(err)
		}
	})
}
