package main

import (
	"encoding/json"
	"fmt"
	"net/url"

	"gitlab.com/gitlab-com/gl-security/product-security/gib/categories"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

// Properties holds the properties of projects
type Properties struct {
	Categories   []categories.Category `json:"categories"`
	URLs         []JSONURL             `json:"urls,omitempty"`
	Dependencies []Dependency          `json:"dependencies,omitempty"`
	// ProtectedBranches is used to create an allow list of values for protected branches.
	// The format is the same as protected branches, without the IDs:
	// https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches
	// When defined, the values will be passed to OPA as part of the project properties,
	// and some policies may apply to validate these values.
	ProtectedBranchesAllowList []*gitlab.ProtectedBranch `json:"protected_branches_allow_list,omitempty"`
}

// PropertiesFile is the name of the projects properties file
const PropertiesFile = "properties.yml"

// JSONURL is used to marshal/unmarshal URLs
type JSONURL struct {
	*url.URL
}

// UnmarshalJSON parses the URL represented as a string
func (j *JSONURL) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	url, err := url.Parse(s)
	if err != nil {
		return err
	}

	if url.Host == "" {
		return fmt.Errorf("invalid url, hostname is empty")
	}
	j.URL = url
	return nil
}

// MarshalJSON marshals the string representation of the underlying URL
func (j JSONURL) MarshalJSON() ([]byte, error) {
	var s string
	if j.URL != nil {
		s = j.String()
	}
	return json.Marshal(s)
}
