package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/hashicorp/go-retryablehttp"
)

// GraphQLClient is an HTTP client used to query the GraphQL API
type GraphQLClient struct {
	client *retryablehttp.Client
	url    string
	token  string
}

// NewGraphQLClient instantiates a new client from a GitLab URL and a Personal Access Token
func NewGraphQLClient(token string, url string) *GraphQLClient {
	client := retryablehttp.NewClient()
	client.RetryMax = 3
	client.RetryWaitMin = 5 * time.Second
	client.RetryWaitMax = 30 * time.Second
	client.Logger = nil

	return &GraphQLClient{
		client: client,
		url:    url,
		token:  token,
	}
}

// GraphQLRequest hold query and variables for GraphQL requests
type GraphQLRequest struct {
	Variables map[string]interface{} `json:"variables"`
	Query     string                 `json:"query"`
}

// GraphQLResponse is used to Unmarshal the response and its potential errors
type GraphQLResponse struct {
	Data   interface{} `json:"data"`
	Errors []struct {
		Message string `json:"message"`
	} `json:"errors"`
}

// ExecuteQuery executes a GraphQL Query with the given variables and set the resulting object to the
// reference passed in parameter v
func (c *GraphQLClient) ExecuteQuery(query string, variables map[string]interface{}, v any) error {
	req := GraphQLRequest{
		Query:     query,
		Variables: variables,
	}

	body, err := json.Marshal(req)
	if err != nil {
		return err
	}

	httpReq, err := retryablehttp.NewRequest("POST", c.url, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	httpReq.Header.Set("Content-Type", "application/json")
	httpReq.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))

	resp, err := c.client.Do(httpReq)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%s non-200 status code: %d", c.url, resp.StatusCode)
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var graphResp GraphQLResponse
	err = json.Unmarshal(respBody, &graphResp)
	if err != nil {
		return err
	}

	if len(graphResp.Errors) > 0 {
		return fmt.Errorf("graphql errors: %+v", graphResp.Errors)
	}

	return json.Unmarshal(respBody, &v)
}
