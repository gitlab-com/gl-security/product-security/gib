# API fixtures

This folder contains cached responses from the GitLab API.

Don't use any Personal Access Token to update this data, to avoid leaking
confidential data.

