package main

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"sync"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

// If new files are added to this list of constants, consider adding then to `$GIT_IGNORE` in `ci/Inventory-Builder.gitlab-ci.yml`
const (
	// ProjectMetadataFile is the path of the projects metadata json file
	ProjectMetadataFile = "project.json"
	// ShortProjectMetadataFile is the path of the projects metadata json file with only essential fields to be committed in the data folder
	ShortProjectMetadataFile = "project-short.json"
	// GroupMetadataFile is the path of the groups metadata json file
	GroupMetadataFile = "group.json"
	// IgnoreFile is the name of the ignore files
	IgnoreFile = "ignore"
	// DependenciesFile is the name of the dependency report of a project
	DependenciesFile = "dependencies.json"
	// VulnerabilitiesFile is the name of the vulnerability report of a project
	VulnerabilitiesFile = "vulnerabilities.json"
	// CIConfigFile is the name of the CI configuration file of a project
	CIConfigFile = "ci-config.json"
	// ViolationsFile is the output of `opa eval`, with policy violations
	ViolationsFile = "violations.json"
	// ProtectedBranchesFile is the name of the protected branches file for a project
	ProtectedBranchesFile = "protected_branches.json"
	// ApprovalConfigFile is the name of the approval configuration file for a project
	ApprovalConfigFile = "approvals.json"
	// ApprovalRulesFile is the name of the approval rules file for a project
	ApprovalRulesFile = "approval_rules.json"
	// CICDSettingsFile is the name of the CI/CD settings file for a project
	CICDSettingsFile = "cicd_settings.json"
	// SyncTokenFile contains a Personal/Project/Group Access Token name to be used for syncing the local tree
	SyncTokenFile = "sync_token_name"
	// DockerImageRequestsFile contains all images requested by CI/CD jobs for a project
	// TODO: Move to project.go
	DockerImageRequestsFile = "images-requested.json"
	// Project job_token_scope allowlist - https://docs.gitlab.com/api/project_job_token_scopes/#get-a-projects-cicd-job-token-inbound-allowlist
	JobTokenScopeAllowlistFile = "job_token_scope_allowlist.json"
)

func syncCmd(c *cli.Context) error {
	git := newClient(c)

	// Walk the data tree
	dataDir := filepath.Clean(c.String(dataDirFlagName))
	dir := c.Args().First()
	if dir == "" {
		dir = dataDir
	}

	s, err := newSyncHandler((git))
	if err != nil {
		return err
	}

	err = walkDataDir(dir, dataDir, s)
	if err != nil {
		return fmt.Errorf("error walking the path %q: %v", dir, err)
	}
	return nil
}

// syncHandler implements the DataDirHandler interface
type syncHandler struct {
	gitLabClient *GitLabClient
}

func newSyncHandler(git *GitLabClient) (*syncHandler, error) {
	if git == nil {
		return nil, fmt.Errorf("can't create syncHandler with nil *GitlabClient")
	}
	return &syncHandler{gitLabClient: git}, nil
}

func (h syncHandler) GitLabClient() *GitLabClient {
	return h.gitLabClient
}

func (h syncHandler) HandleSingleProject(dataDir string, project *Project, glClient *GitLabClient) error {
	// Force refresh from the API
	project.ID = 0
	return project.Sync(glClient)
}

func (h syncHandler) HandleProjects(dataDir, path string, git *GitLabClient) error {
	return syncPath(path, dataDir, git)
}

func cleanupProjects(projects []*Project, path string) error {
	files, err := os.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	// clean-up projects
CLEANUP:
	for _, f := range files {
		projectDir := filepath.Join(path, f.Name())
		if !f.IsDir() {
			continue
		}
		projectFiles, err := os.ReadDir(projectDir)
		if err != nil {
			return err
		}

		if !isProject(f, projectFiles) {
			continue
		}

		for _, project := range projects {
			if f.Name() == project.Path {
				continue CLEANUP
			}
		}

		log.WithFields(log.Fields{
			"name": f.Name(),
			"path": f.Name(),
		}).Info("Removing deleted or archived project")
		err = os.RemoveAll(projectDir)
		if err != nil {
			return err
		}
	}
	return nil
}

func syncPath(path, dataDir string, git *GitLabClient) error {
	files, err := os.ReadDir(path)
	if err != nil {
		return err
	}

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return err
	}

	if !isGroup(fs.FileInfoToDirEntry(fileInfo), files) {
		return nil
	}

	group, err := filepath.Rel(dataDir, path)
	if err != nil {
		return err
	}

	projects, err := updateGroupProjects(path, dataDir, files, group, git)
	if err != nil {
		return err
	}

	err = cleanupProjects(projects, path)
	if err != nil {
		return err
	}

	if !hasIgnoreFile(files) {
		err := updateSubGroups(path, group, git)
		if err != nil {
			return err
		}
	}
	return nil
}

func updateSubGroups(path, group string, git *GitLabClient) error {
	log.WithField("group", group).Info("Fetching subgroups")

	// Update subgroups
	page := 1
	var subgroups []*Group
	for page != 0 {
		listOpts := &gitlab.ListSubGroupsOptions{
			ListOptions: gitlab.ListOptions{
				PerPage: 100,
				Page:    page,
			},
		}

		s, response, err := git.Groups.ListSubGroups(group, listOpts)
		if err != nil {
			return err
		}

		page = response.NextPage
		for _, subgroup := range s {
			subgroups = append(subgroups, &Group{StoragePath: filepath.Join(path, subgroup.Path), Group: subgroup})
		}
	}

	log.WithFields(log.Fields{
		"group": group,
	}).Infof("Found %d subgroup(s)", len(subgroups))

	for _, subgroup := range subgroups {
		err := subgroup.Save()
		if err != nil {
			return err
		}
	}

	return cleanupGroups(path, subgroups)
}

func cleanupGroups(path string, subgroups []*Group) error {
	// clean-up groups
	files, err := os.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

CLEANUP:
	for _, f := range files {
		if !f.IsDir() {
			continue
		}

		groupDir := filepath.Join(path, f.Name())
		groupFiles, err := os.ReadDir(groupDir)
		if err != nil {
			return err
		}

		if !isGroup(f, groupFiles) {
			continue
		}

		for _, group := range subgroups {
			if f.Name() == group.Path {
				continue CLEANUP
			}
		}

		log.WithFields(log.Fields{
			"path":  groupDir,
			"group": f.Name(),
		}).Info("Removing deleted group")
		err = os.RemoveAll(groupDir)
		if err != nil {
			return err
		}
	}
	return nil
}

func updateGroupProjects(path, dataDir string, files []fs.DirEntry, group string, git *GitLabClient) ([]*Project, error) {
	var projects []*Project

	if hasIgnoreFile(files) {
		// If the current folder has an ignore file, we need to check if some projects
		// are present, and sync them one by one.
		for _, f := range files {
			if !f.IsDir() {
				continue
			}
			projectPath := filepath.Join(path, f.Name())
			pFiles, err := os.ReadDir(projectPath)
			if err != nil {
				return projects, err
			}

			if isProject(f, pFiles) {
				projectNS, err := filepath.Rel(dataDir, projectPath)
				if err != nil {
					return projects, err
				}
				project := &Project{
					StoragePath: projectPath,
					Project: &gitlab.Project{
						PathWithNamespace: projectNS,
					},
				}
				projects = append(projects, project)
			}
		}
	} else {
		// Otherwise, we fetch all the projects of the current group
		log.WithField("group", group).Info("Fetching group projects")

		page := 1
		for page != 0 {
			listOpts := &gitlab.ListGroupProjectsOptions{
				WithShared: gitlab.Ptr(false),
				ListOptions: gitlab.ListOptions{
					PerPage: 100,
					Page:    page,
				},
			}

			p, response, err := git.Groups.ListGroupProjects(group, listOpts)
			if err != nil {
				return projects, err
			}
			page = response.NextPage
			for _, project := range p {
				if project.Archived {
					log.WithFields(log.Fields{
						"name":  project.Name,
						"path":  project.Path,
						"group": group,
					}).Debug("Skipping archived project")
					continue
				}
				projects = append(projects, &Project{StoragePath: filepath.Join(path, project.Path), Project: project})
			}
		}
		log.WithField("group", group).Infof("Found %d project(s)", len(projects))
	}

	// Sync all projects concurrently
	if err := syncProjectsConcurrently(projects, git); err != nil {
		return projects, err
	}

	return projects, nil
}

// syncProjectsConcurrently handles concurrent synchronization of multiple projects
func syncProjectsConcurrently(projects []*Project, git *GitLabClient) error {
	// Create error channel and WaitGroup for concurrent execution
	errChan := make(chan error, len(projects))
	var wg sync.WaitGroup

	// Launch a goroutine for each project
	for _, project := range projects {
		wg.Add(1)
		go func(p *Project) {
			defer wg.Done()
			if err := p.Sync(git); err != nil {
				errChan <- fmt.Errorf("error syncing project %s: %w", p.Project.PathWithNamespace, err)
			}
		}(project)
	}

	// Wait for all goroutines to complete
	wg.Wait()
	close(errChan)

	// Check for any errors
	for err := range errChan {
		if err != nil {
			return err
		}
	}

	return nil
}
