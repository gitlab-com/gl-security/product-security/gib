package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/gitlab-com/gl-security/product-security/gib/categories"
	"sigs.k8s.io/yaml"
)

func TestCategories(t *testing.T) {
	b := []byte(
		`
categories:
  - product
  - library
  - website
  - api
  - service
  - deploy
  - internal
  - external
  - green_data
  - yellow_data
  - orange_data
  - red_data
  - 3rd_party
  - demo
  - test
  - poc
  - team
  - temporary
  - deprecated
  - use_pat
  - marked_for_deletion
  - keep_private
  - docs
  - tooling
  - container
  - fork
  - secrets_monitoring
  - security_policy_project
`)
	var p Properties
	err := yaml.Unmarshal(b, &p)
	if err != nil {
		t.Error("Expected err to be nil, got: ", err)
	}
}

func TestURLs(t *testing.T) {
	b := []byte(
		`
urls:
  - https://gitlab.com
  - https://gitlab.com/api/v4
`)
	var p Properties
	err := yaml.Unmarshal(b, &p)
	if err != nil {
		t.Error("Expected err to be nil, got: ", err)
	}
}

func TestProtectedBranches(t *testing.T) {
	b := []byte(
		`
categories:
  - product

protected_branches_allow_list:
  - name: main
    push_access_levels:
      - access_level: 40
        access_level_description: "Maintainers"
        user_id: null
        group_id: null
`)
	var p Properties
	err := yaml.Unmarshal(b, &p)
	if err != nil {
		t.Error("Expected err to be nil, got: ", err)
	}
}

func TestSoftwareSystems(t *testing.T) {
	b := []byte(
		`
categories:
  - product
  - system::gitlab-runner
`)
	var p Properties
	err := yaml.Unmarshal(b, &p)
	if err != nil {
		t.Error("Expected err to be nil, got: ", err)
	}
	if diff := cmp.Diff([]categories.Category{categories.Product, categories.Category("system::gitlab-runner")}, p.Categories); diff != "" {
		t.Errorf("Categories mismatch (-want +got):\n%s", diff)
	}
}

func TestEmptySoftwareSystem(t *testing.T) {
	b := []byte(
		`
categories:
  - product
  - "system::"
`)
	var p Properties
	err := yaml.Unmarshal(b, &p)
	if err == nil {
		t.Error("Expected err to be nil, but got: ", err)
	}
}

func TestArchs(t *testing.T) {
	b := []byte(
		`
categories:
  - product
  - arch:ra1k_omnibus
`)
	var p Properties
	err := yaml.Unmarshal(b, &p)
	if err != nil {
		t.Error("Expected err to be nil, got: ", err)
	}
	if diff := cmp.Diff([]categories.Category{categories.Product, categories.Category("arch:ra1k_omnibus")}, p.Categories); diff != "" {
		t.Errorf("Categories mismatch (-want +got):\n%s", diff)
	}
}

func TestEmptyArch(t *testing.T) {
	b := []byte(
		`
categories:
  - product
  - "arch:"
`)
	var p Properties
	err := yaml.Unmarshal(b, &p)
	if err == nil {
		t.Error("Expected err to be nil, but got: ", err)
	}
}
