package main

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

// DataDirHandler wraps methods used by the walkDataDir function to handle one or many projects, and to provide a GitLabClient
type DataDirHandler interface {
	GitLabClient() *GitLabClient
	HandleSingleProject(string, *Project, *GitLabClient) error
	HandleProjects(string, string, *GitLabClient) error
}

func walkDataDir(dir, dataDir string, h DataDirHandler) error {
	return filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		// Groups and projects are always directories
		if !d.IsDir() ||
			// Root of dataDir is a noop
			path == dataDir {
			return nil
		}

		files, err := os.ReadDir(path)
		if err != nil {
			return err
		}

		// if no local sync token file is found, localGit is set with git (no change)
		localGit, err := setLocalSyncTokenIfPresent(dataDir, path, h.GitLabClient())
		if err != nil {
			return err
		}

		if path == dir && isProject(d, files) {
			project, err := LoadProject(dataDir, path)
			if err != nil {
				return err
			}

			return h.HandleSingleProject(dataDir, project, localGit)
		}

		return h.HandleProjects(dataDir, path, localGit)
	})
}

// hasIgnoreFile returns true if the current directory include `ignore` in its files.
func hasIgnoreFile(files []fs.DirEntry) bool {
	for _, f := range files {
		if !f.IsDir() && f.Name() == IgnoreFile {
			return true
		}
	}
	return false
}

// Search for a sync token in the local directory (path) and all parent directories up to the dataDir root
func setLocalSyncTokenIfPresent(dataDir, path string, git *GitLabClient) (*GitLabClient, error) {
	path = filepath.Clean(path) + string(os.PathSeparator)
	// iterate on directories, from leaf to root
	// Example with path=/data/folderA/folberB/folderC:
	// /data/folderA/folberB/folderC
	// /data/folderA/folberB
	// /data/folderA
	// /data
	for dir, _ := filepath.Split(path); filepath.Clean(dir) != dataDir; dir, _ = filepath.Split(dir[:len(dir)-1]) {
		files, err := os.ReadDir(dir)
		if err != nil {
			return git, err
		}

		// Look up for a token file in the current dir
		for _, f := range files {
			if !f.IsDir() && f.Name() == SyncTokenFile {
				tokenNameInFile, err := os.ReadFile(filepath.Join(dir, f.Name()))
				if err != nil {
					return git, err
				}
				tokenName := strings.TrimSpace(string(tokenNameInFile))
				tokenValue := os.Getenv(tokenName)
				if tokenValue == "" {
					return git, fmt.Errorf("access token %s is empty", tokenName)
				}
				git, err = newClientFrom(git, tokenValue)
				if err != nil {
					return git, err
				}
			}
		}
	}
	return git, nil
}

func isProject(d fs.DirEntry, files []fs.DirEntry) bool {
	if !d.IsDir() {
		return false
	}
	for _, f := range files {
		if !f.IsDir() && (f.Name() == ProjectMetadataFile || f.Name() == PropertiesFile) {
			return true
		}
	}
	return false
}

func isGroup(d fs.DirEntry, files []fs.DirEntry) bool {
	if !d.IsDir() {
		return false
	}
	return !isProject(d, files)
}
