package gitlab.projects.cicd_settings

import rego.v1

import data.gitlab.projects.lib
import input.cicd_settings

violation contains {"description": "CI/CD Job Token access should be restricted.\n\nSee https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#control-job-token-access-to-your-project", "key": "inboundjobtokenscope_not_enabled", "msg": "CI/CD Job Token is not restricted"} if {
	lib.repository_is_active
	not cicd_settings.inboundJobTokenScopeEnabled == true
}
