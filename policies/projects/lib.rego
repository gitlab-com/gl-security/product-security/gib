package gitlab.projects.lib

import rego.v1

import input.categories
import input.project

repository_is_active if {
	not project.empty_repo == true
	not project.repository_access_level == "disabled"
}

critical_project if {
	categories[_] = "product"
}

critical_project if {
	categories[_] = "red_data"
}

critical_project if {
	categories[_] = "library"
}
