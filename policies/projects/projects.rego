package gitlab.projects

import rego.v1

import data.gitlab.projects.approvals
import data.gitlab.projects.cicd_settings
import data.gitlab.projects.invalid_ci_configs
import data.gitlab.projects.pre_receive_secret_detection
import data.gitlab.projects.protected_branches
import data.gitlab.projects.restrict_user_defined_variables
import data.gitlab.projects.security_jobs
import data.gitlab.projects.tidy
import data.gitlab.projects.visibility

violation contains {"description": description, "key": key, "msg": msg} if {
	security_jobs.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	visibility.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	tidy.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	protected_branches.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	approvals.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	invalid_ci_configs.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	pre_receive_secret_detection.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	restrict_user_defined_variables.violation[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	cicd_settings.violation[{"msg": msg, "description": description, "key": key}]
}
