package gitlab.projects.pre_receive_secret_detection

import rego.v1

import data.gitlab.projects.lib
import input.project

violation contains {
	"description": "Secret Push Protection mush be enabled for this project.\n\nSee https://docs.gitlab.com/ee/user/application_security/secret_detection/secret_push_protection/",
	"key": "spp_not_enabled",
	"msg": "Secret Push Protection is not enabled",
} if {
	lib.repository_is_active
	not project.pre_receive_secret_detection_enabled == true
}
