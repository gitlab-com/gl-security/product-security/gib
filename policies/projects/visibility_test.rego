package gitlab.projects.visibility

import rego.v1

test_keep_private_with_public if {
	results := violation with input as {"categories": ["keep_private"], "project": {"visibility": "public"}}
	results == {{"msg": "Project can't be public", "description": "Private project must remain private", "key": "private_project_exposed"}}
}

test_keep_private_with_private if {
	count(violation) == 0 with input as {"categories": ["keep_private"], "project": {"visibility": "private"}}
}

test_project_with_internal_visibility if {
	results := violation with input as {"project": {"visibility": "internal"}}
	results == {{"msg": "Internal visibility", "description": "Internal visibility is forbidden", "key": "internal_visibility"}}
}
