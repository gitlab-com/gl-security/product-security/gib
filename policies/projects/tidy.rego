package gitlab.projects.tidy

import rego.v1

marked_for_deletion if {
	input.categories[_] == "marked_for_deletion"
}

violation contains {"description": "Project marked for deletion", "key": "marked_for_deletion", "msg": "Must be deleted"} if {
	marked_for_deletion
}

violation contains {"description": "Project is deprecated and must be archived", "key": "marked_for_archiving", "msg": "Must be archived"} if {
	input.categories[_] == "deprecated"

	# marked_for_deletion has priority
	not marked_for_deletion
}

# TODO: temporary projects should be deleted after time
