package gitlab.projects.security_jobs

import rego.v1

all_jobs_configured(category) := output if {
	output = {
		"categories": [category],
		"ci-config": {"merged_yaml": {
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
		}},
	}
}

all_jobs_configured_with_advanced_sast(category) := output if {
	output = {
		"categories": [category],
		"ci-config": {"merged_yaml": {
			"variables": {"GITLAB_ADVANCED_SAST_ENABLED": "true"},
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
		}},
	}
}

result_no_jobs_configured := {
	{"description": "SAST is not configured", "key": "sast_not_configured", "msg": "SAST is not configured"},
	{"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"},
	{"description": "Secret Detection is not configured", "key": "secret_detection_not_configured", "msg": "Secret Detection is not configured"},
}

result_ds_sd_not_configured := {
	{"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"},
	{"description": "Secret Detection is not configured", "key": "secret_detection_not_configured", "msg": "Secret Detection is not configured"},
}

test_product_with_no_security_jobs if {
	results := violation with input as {"categories": ["product"], "ci-config": {}}
	results == result_no_jobs_configured
}

test_red_data_with_no_security_jobs if {
	results := violation with input as {"categories": ["red_data"], "ci-config": {}}
	results == result_no_jobs_configured
}

test_library_with_no_security_jobs if {
	results := violation with input as {"categories": ["library"], "ci-config": {}}
	results == result_no_jobs_configured
}

test_product_with_hidden_jobs if {
	project_input := {
		"categories": ["product"],
		"ci-config": {"merged_yaml": {
			".bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			".gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			".secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
			".container-scanning": {"artifacts": {"reports": {"container_scanning": ["gl-container-scanning-report.json"]}}},
		}},
	}
	results := violation with input as project_input
	results == result_no_jobs_configured
}

test_product_and_container_with_no_security_jobs if {
	results := violation with input as {"categories": ["product", "container"], "ci-config": {}}
	results == {
		{"description": "SAST is not configured", "key": "sast_not_configured", "msg": "SAST is not configured"},
		{"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"},
		{"description": "Secret Detection is not configured", "key": "secret_detection_not_configured", "msg": "Secret Detection is not configured"},
		{"description": "Container Scanning is not configured", "key": "container_scanning_not_configured", "msg": "Container Scanning is not configured"},
	}
}

test_other_category_with_no_security_jobs if {
	results := violation with input as {"categories": ["marked_for_deletion"], "ci-config": {}}
	results == set()
}

test_product_with_all_jobs_configured if {
	project_input := all_jobs_configured("product")
	results := violation with input as project_input

	# Should have advanced SAST violation
	results == {{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"}}
}

test_product_with_all_jobs_configured_and_advanced_sast if {
	project_input := all_jobs_configured_with_advanced_sast("product")
	results := violation with input as project_input

	# No violations
	results == set()
}

test_product_and_container_with_all_jobs_configured if {
	project_input := {
		"categories": ["product", "container"],
		"ci-config": {"merged_yaml": {
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
			"container-scanning": {"artifacts": {"reports": {"container_scanning": ["gl-container-scanning-report.json"]}}},
		}},
	}
	results := violation with input as project_input

	# Should have advanced SAST violation
	results == {{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"}}
}

test_product_and_container_with_all_jobs_configured_and_advanced_sast if {
	project_input := {
		"categories": ["product", "container"],
		"ci-config": {"merged_yaml": {
			"variables": {"GITLAB_ADVANCED_SAST_ENABLED": "true"},
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
			"container-scanning": {"artifacts": {"reports": {"container_scanning": ["gl-container-scanning-report.json"]}}},
		}},
	}
	results := violation with input as project_input

	# No violations
	results == set()
}

# Container Scanning now generates two reports, but still want Dependency Scanning as well
test_product_with_all_jobs_configured_except_dependency_scanning if {
	project_input := {
		"categories": ["product"],
		"ci-config": {"merged_yaml": {
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
			"container-scanning": {"artifacts": {"reports": {
				"container_scanning": ["gl-container-scanning-report.json"],
				"dependency_scanning": ["gl-dependency-scanning-report.json"],
			}}},
		}},
	}
	results := violation with input as project_input

	# Should have dependency scanning and advanced SAST violations
	results == {
		{"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"},
		{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"},
	}
}

test_red_data_with_all_jobs_configured if {
	project_input := all_jobs_configured("red_data")
	results := violation with input as project_input

	# Should have advanced SAST violation
	results == {{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"}}
}

test_red_data_with_all_jobs_configured_and_advanced_sast if {
	project_input := all_jobs_configured_with_advanced_sast("red_data")
	results := violation with input as project_input

	# No violations
	results == set()
}

test_library_with_all_jobs_configured if {
	project_input := all_jobs_configured("library")
	results := violation with input as project_input

	# Should have advanced SAST violation
	results == {{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"}}
}

test_library_with_all_jobs_configured_and_advanced_sast if {
	project_input := all_jobs_configured_with_advanced_sast("library")
	results := violation with input as project_input

	# No violations
	results == set()
}

test_product_with_jobs_disabled if {
	project_input := {
		"categories": ["product", "container"],
		"ci-config": {"merged_yaml": {
			"variables": {
				"SAST_DISABLED": "true",
				"DEPENDENCY_SCANNING_DISABLED": "true",
				"SECRET_DETECTION_DISABLED": "true",
				"CONTAINER_SCANNING_DISABLED": "true",
			},
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
			"container-scanning": {"artifacts": {"reports": {"container_scanning": ["gl-container-scanning-report.json"]}}},
		}},
	}
	results := violation with input as project_input
	results == {
		{"description": "DEPENDENCY_SCANNING_DISABLED is set to \"true\"", "key": "dependency_scanning_is_disabled", "msg": "DEPENDENCY_SCANNING_DISABLED is not 'false'"},
		{"description": "SAST_DISABLED is set to \"true\"", "key": "sast_is_disabled", "msg": "SAST_DISABLED is not 'false'"},
		{"description": "SECRET_DETECTION_DISABLED is set to \"true\"", "key": "secret_detection_is_disabled", "msg": "SECRET_DETECTION_DISABLED is not 'false'"},
		{"description": "CONTAINER_SCANNING_DISABLED is set to \"true\"", "key": "container_scanning_is_disabled", "msg": "CONTAINER_SCANNING_DISABLED is not 'false'"},
		{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"},
	}
}

test_sast_enabled_but_advanced_sast_disabled if {
	project_input := {
		"categories": ["product"],
		"ci-config": {"merged_yaml": {
			"variables": {"GITLAB_ADVANCED_SAST_ENABLED": "false"},
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
		}},
	}
	results := violation with input as project_input

	# Should have advanced SAST violation
	results == {{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"}}
}

test_sast_enabled_with_advanced_sast_not_set if {
	project_input := {
		"categories": ["product"],
		"ci-config": {"merged_yaml": {
			"variables": {},
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
		}},
	}
	results := violation with input as project_input

	# Should have advanced SAST violation
	results == {{"description": "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'", "key": "advanced_sast_not_enabled", "msg": "Advanced SAST is not enabled"}}
}

#
# `use_pat`, `website`+`external` | [Dependency Scanning] and [Secret Detection] must be enabled |
#
test_use_pat_with_no_security_jobs if {
	results := violation with input as {"categories": ["use_pat"], "ci-config": {}}
	results == result_ds_sd_not_configured
}

test_use_pat_with_all_jobs_configures if {
	project_input := all_jobs_configured("use_pat")
	results := violation with input as project_input
	results == set()
}

test_website_external_with_no_security_jobs if {
	results := violation with input as {"categories": ["website", "external"], "ci-config": {}}
	results == result_ds_sd_not_configured
}

test_use_website_external_with_all_jobs_configures if {
	project_input := {
		"categories": ["website", "external"],
		"ci-config": {"merged_yaml": {
			"bandit-sast": {"artifacts": {"reports": {"sast": ["gl-sast-report.json"]}}},
			"gemnasium-python": {"artifacts": {"reports": {"dependency_scanning": ["gl-dependency-scanning-report.json"]}}},
			"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}},
		}},
	}

	results := violation with input as project_input
	results == set()
}

test_secret_detection_historic_scan_is_set if {
	project_input := {"ci-config": {"merged_yaml": {"variables": [{"SECRET_DETECTION_HISTORIC_SCAN": "true"}]}}}

	results := violation with input as project_input
	results == {{"description": "SECRET_DETECTION_HISTORIC_SCAN is set to \"true\"", "key": "secrets_historic_scan_is_enabled", "msg": "SECRET_DETECTION_HISTORIC_SCAN is not 'false'"}}
}

test_secret_detection_historic_scan_is_set_in_job if {
	project_input := {"ci-config": {"merged_yaml": {"secret-detection": {
		"variables": [{"SECRET_DETECTION_HISTORIC_SCAN": "true"}],
		"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}},
	}}}}

	results := violation with input as project_input
	results == {{"description": "SECRET_DETECTION_HISTORIC_SCAN is set to \"true\"", "key": "secrets_historic_scan_is_enabled", "msg": "SECRET_DETECTION_HISTORIC_SCAN is not 'false'"}}
}

test_docs_without_secret_detection if {
	results := violation with input as {
		"categories": ["docs"],
		"ci-config": {"merged_yaml": {"secret-detection": {"artifacts": {"reports": {"secret_detection": ["gl-secret-detection-report.json"]}}}}},
	}
	results == set()
}

test_docs_with_secret_detection if {
	results := violation with input as {"categories": ["docs"]}
	results == {{"description": "Secret Detection is not configured", "key": "secret_detection_not_configured", "msg": "Secret Detection is not configured"}}
}
