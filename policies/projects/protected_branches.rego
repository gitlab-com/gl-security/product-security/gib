package gitlab.projects.protected_branches

import rego.v1

import data.gitlab.projects.lib
import input.project
import input.protected_branches
import input.protected_branches_allow_list

default_branch_is_protected if {
	default_branch := project.default_branch
	glob.match(protected_branches[_].name, ["*"], default_branch)
}

violation contains {"description": description, "key": key, "msg": msg} if {
	not default_branch_is_protected
	lib.repository_is_active
	msg = "Default branch not protected"
	description = "Default branch not protected"
	key = "default_branch_not_protected"
}

push_access_levels := [x |
	protected_branches[i].name == project.default_branch
	x := protected_branches[i].push_access_levels[_]
]

violation contains {"description": description, "key": key, "msg": msg} if {
	lib.critical_project
	users_can_push
	lib.repository_is_active
	description := users_can_push_description[_]
	msg = "Users can push"
	key = "users_can_push"
}

users_can_push if {
	not protected_branches_allow_list
	push_access_levels[_].access_level != 0 # No one is allowed to push to the default branch, see #71
}

users_can_push if {
	protected_branches_allow_list
	protected_branches_allow_list[i].name == project.default_branch
	push_access_levels != protected_branches_allow_list[i].push_access_levels
}

users_can_push_description contains description if {
	protected_branches_allow_list
	protected_branches_allow_list[i].name == project.default_branch
	users := [user | user := push_access_levels[_].access_level_description]
	allowed_users := [user | user := protected_branches_allow_list[i].push_access_levels[_].access_level_description]
	description := sprintf("Users can push to default branch: %v (allowed: %v)", [users, allowed_users])
}

users_can_push_description contains description if {
	not protected_branches_allow_list
	users := [user | user := push_access_levels[i].access_level_description]
	description := sprintf("Users can push to default branch: %v", [users])
}

violation contains {"description": description, "key": key, "msg": msg} if {
	lib.critical_project
	protected_branches[i].name == project.default_branch
	protected_branches[i].merge_access_levels[_].access_level == 30 # Developers + Maintainers
	lib.repository_is_active
	msg = "Developers can merge"
	description = "Developers can merge to default branch"
	key = "developers_can_merge"
}
