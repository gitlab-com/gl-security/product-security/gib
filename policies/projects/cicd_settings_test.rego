package gitlab.projects.cicd_settings

import rego.v1

test_dont_report_inbound_cicd_job_token_scope_enabled if {
	results := violation with input as {"project": {"empty_repo": false, "repository_access_level": "enabled"}, "cicd_settings": {"inboundJobTokenScopeEnabled": true}}
	results == set()
}

test_report_pre_receive_inbound_cicd_job_token_scope_disabled if {
	results := violation with input as {"project": {"empty_repo": false, "repository_access_level": "enabled"}, "cicd_settings": {"inboundJobTokenScopeEnabled": false}}
	results == {{
		"msg": "CI/CD Job Token is not restricted",
		"description": "CI/CD Job Token access should be restricted.\n\nSee https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#control-job-token-access-to-your-project",
		"key": "inboundjobtokenscope_not_enabled",
	}}
}

test_dont_report_for_empty_repos if {
	results := violation with input as {"project": {"empty_repo": true}}
	results == set()
}

test_dont_report_for_disabled_repos if {
	results := violation with input as {"project": {"repository_access_level": "disabled"}}
	results == set()
}
