package gitlab.projects.invalid_ci_configs

import rego.v1

import data.gitlab.projects.lib
import input["ci-config"] as ci_config

violation contains {"description": description, "key": key, "msg": msg} if {
	not ci_config.valid == true
	lib.repository_is_active
	not ci_config.errors == ["Please provide content of .gitlab-ci.yml"]
	msg = "Invalid CI/CD configuration"
	description = sprintf("Project CI/CD configuration is invalid. This means some jobs we might expect to run are not running. Errors: %v", [get(ci_config, "errors", null)])
	key = "invalid_ci_config"
}

get(value, field, _) := value[field]

get(value, field, orElse) := orElse if {
	not defined(value, field)
}

defined(value, field) if {
	_ = value[field]
}
