package gitlab.vulnerabilities

import rego.v1

test_project_with_vulns_over_180_days if {
	vulnerabilities := [
		{
			"state": "detected",
			"created_at": "2000-11-30T18:41:16.617Z",
		},
		{
			"state": "resolved",
			"created_at": "2000-11-30T18:41:16.617Z",
		},
	]

	result := violation with input as vulnerabilities
	result == {{"msg": "Vulnerabilities over SLO (180 days)", "description": "1 vulnerabilities over SLO (180 days)", "key": "vulnerabilities_over_slo"}}
}

test_project_without_vulns_over_180_days if {
	vulnerabilities := [
		{
			"state": "confirmed",
			"created_at": "2000-11-30T18:41:16.617Z",
		},
		{
			"state": "resolved",
			"created_at": "2000-11-30T18:41:16.617Z",
		},
	]

	result := violation with input as vulnerabilities
	count(result) == 0
}

test_project_with_package_hunter_findings if {
	mock_current_time := time.parse_rfc3339_ns("2000-11-30T18:41:16.617Z")

	vulnerabilities := [
		{
			"state": "detected",
			"created_at": "2000-11-28T18:41:16.617Z",
			"scanner": {"id": "packagehunter"},
		},
		{
			"state": "detected",
			"created_at": "2000-11-28T18:41:16.617Z",
			"scanner": {"id": "anotherscanner"},
		},
	]

	result := violation with current_time as mock_current_time
		with input as vulnerabilities
	result == {{"msg": "Untriaged Package Hunter finding", "description": "1 untriaged Package Hunter findings", "key": "untriaged_package_hunter"}}
}

test_project_with_same_day_package_hunter_findings if {
	mock_current_time := time.parse_rfc3339_ns("2000-11-30T18:41:16.617Z")

	vulnerabilities := [{
		"state": "detected",
		"created_at": "2000-11-30T18:41:16.617Z",
		"scanner": {"id": "packagehunter"},
	}]

	result := violation with current_time as mock_current_time
		with input as vulnerabilities
	count(result) == 0
}

test_project_with_triaged_package_hunter_findings if {
	mock_current_time := time.parse_rfc3339_ns("2000-11-30T18:41:16.617Z")

	vulnerabilities := [{
		"state": "resolved",
		"created_at": "2000-11-28T18:41:16.617Z",
		"scanner": {"id": "packagehunter"},
	}]

	result := violation with current_time as mock_current_time
		with input as vulnerabilities
	count(result) == 0
}
