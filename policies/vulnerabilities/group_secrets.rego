# Secrets defined (as CI/CD variables) at the group level are inherited in sub-groups and projects.
# While this behaviour is expected, it can lead to security issues when these
# variables are exposed in project and some team members are not supposed to have
# access to them.
#
# This policy detects vulnerabilities reported by projects mirroring:
# https://gitlab.com/gitlab-com/gl-security/appsec/tooling/group-variables-monitoring/
# See https://gitlab.com/gitlab-com/gl-security/product-security/gib/-/issues/64

package gitlab.vulnerabilities

import rego.v1

group_secrets contains vulnerability if {
	vulnerability := input[_]
	allowed_states := {"detected", "confirmed"}
	vulnerability.state == allowed_states[_]
	vulnerability.scanner.id == "semgrep"
	vulnerability.identifiers[_].value == "group_ci_cd_variables"
}

violation contains {"description": description, "key": "detected_group_secret", "msg": msg} if {
	vulnerability := group_secrets[_]
	msg := sprintf("Untriaged Group CI/CD secret (id: %d)", [vulnerability.id])
	description := sprintf("Untriaged Group CI/CD vulnerability: %s", [vulnerability.title])
}
