package gitlab.vulnerabilities

import rego.v1

test_project_with_detected_glpat if {
	vulnerabilities := [
		{
			"id": 1,
			"state": "detected",
			"description": "GitLab Personal Access Token",
		},
		{
			"id": 2,
			"state": "dismissed",
			"description": "GitLab Personal Access Token",
		},
		{
			"id": 3,
			"state": "detected",
			"description": "GitLab Personal Access Token",
		},
	]

	result := violation with input as vulnerabilities
	result == {
		{"msg": "Untriaged GLPAT secret (id: 1)", "description": "Untriaged Secret Detection findings related to a GitLab Personal Access Token (id: 1)", "key": "detected_glpat"},
		{"msg": "Untriaged GLPAT secret (id: 3)", "description": "Untriaged Secret Detection findings related to a GitLab Personal Access Token (id: 3)", "key": "detected_glpat"},
	}
}

test_project_without_detected_glpat if {
	vulnerabilities := [
		{
			"id": 1,
			"state": "confirmed",
			"description": "GitLab Personal Access Token",
		},
		{
			"id": 2,
			"state": "resolved",
			"description": "GitLab Personal Access Token",
		},
	]

	result := violation with input as vulnerabilities
	count(result) == 0
}
