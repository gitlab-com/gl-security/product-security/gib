# Vulnerabilities must be triaged under 180 days
# See https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91396

package gitlab.vulnerabilities

import rego.v1

current_time := time.now_ns()

detected_vulnerabilites contains vulnerability if {
	vulnerability := input[_]
	vulnerability.state == "detected"
}

over_slo contains vulnerability if {
	vulnerability := detected_vulnerabilites[_]
	ns := time.parse_rfc3339_ns(vulnerability.created_at)
	ns <= time.add_date(current_time, 0, 0, -180)
}

day_old contains vulnerability if {
	vulnerability := detected_vulnerabilites[_]
	ns := time.parse_rfc3339_ns(vulnerability.created_at)
	ns <= time.add_date(current_time, 0, 0, -1)
}

package_hunter_findings contains vulnerability if {
	vulnerability := day_old[_]
	vulnerability.scanner.id == "packagehunter"
}

violation contains {"description": description, "key": "vulnerabilities_over_slo", "msg": msg} if {
	count(over_slo) > 0
	msg := "Vulnerabilities over SLO (180 days)"
	description := sprintf("%d vulnerabilities over SLO (180 days)", [count(over_slo)])
}

violation contains {"description": description, "key": "untriaged_package_hunter", "msg": msg} if {
	count(package_hunter_findings) > 0
	msg := "Untriaged Package Hunter finding"
	description := sprintf("%d untriaged Package Hunter findings", [count(package_hunter_findings)])
}
