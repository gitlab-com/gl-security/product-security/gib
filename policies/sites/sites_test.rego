package gitlab.sites

import rego.v1

test_site_with_b_grade if {
	ssl_input := [
		{
			"id": "overall_grade",
			"ip": "customers.gitlab.com/104.18.27.123",
			"port": "443",
			"severity": "OK",
			"finding": "B",
		},
		{
			"id": "overall_grade",
			"ip": "customers.gitlab.com/104.18.26.123",
			"port": "443",
			"severity": "OK",
			"finding": "A+",
		},
	]

	results := violation with input as ssl_input
	results == {{"msg": "Grade should be at least \"A\"", "description": "Grade should be at least \"A\", got \"B\"", "key": "grade_not_A"}}
}

test_site_with_b_and_c_grades if {
	ssl_input := [
		{
			"id": "overall_grade",
			"ip": "customers.gitlab.com/104.18.27.123",
			"port": "443",
			"severity": "OK",
			"finding": "B",
		},
		{
			"id": "overall_grade",
			"ip": "customers.gitlab.com/104.18.26.123",
			"port": "443",
			"severity": "OK",
			"finding": "A+",
		},
		{
			"id": "overall_grade",
			"ip": "customers.gitlab.com/104.18.25.123",
			"port": "443",
			"severity": "OK",
			"finding": "C",
		},
	]

	results := violation with input as ssl_input
	results == {{"msg": "Grade should be at least \"A\"", "description": "Grade should be at least \"A\", got [\"B\", \"C\"]", "key": "grade_not_A"}}
}

test_site_with_a_grade if {
	ssl_input := [{
		"id": "overall_grade",
		"ip": "customers.gitlab.com/104.18.27.123",
		"port": "443",
		"severity": "OK",
		"finding": "A+",
	}]

	count(violation) == 0 with input as ssl_input
}
