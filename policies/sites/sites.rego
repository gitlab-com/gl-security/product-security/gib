package gitlab.sites

import rego.v1

grades contains g if {
	input[i].id == "overall_grade"
	g := input[i].finding
}

grades_not_A := g if {
	s := grades - {"A+", "A", "A-"}
	g := [x | x = s[_]]
}

violation contains {"description": description, "key": "grade_not_A", "msg": "Grade should be at least \"A\""} if {
	count(grades_not_A) == 1
	description := sprintf("Grade should be at least \"A\", got %q", [grades_not_A[0]])
}

violation contains {"description": description, "key": "grade_not_A", "msg": "Grade should be at least \"A\""} if {
	count(grades_not_A) > 1
	description := sprintf("Grade should be at least \"A\", got %v", [grades_not_A])
}
