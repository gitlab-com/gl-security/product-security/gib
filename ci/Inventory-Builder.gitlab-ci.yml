#
# /!\ DEPRECATION NOTICE /!\
# This template is deprecated, please use the "inventory" component of this project instead
#
#
# Read more about this feature here:
# https://gitlab.com/gitlab-com/gl-security/product-security/gib
#

stages:
  - validate
  - sync
  - ssl-check
  - compliance
  - update
  - create-issues
  - report
  - commit

variables:
  # sane defaults for GitLab. Use a different account on your instance.
  GIT_USER_EMAIL: "gitlab-bot@gitlab.com"
  GIT_USER_NAME: "GitLab Bot"
  GIT_CREDENTIAL_USERNAME: "gitlab-bot"
  # Some files change a lot, and storing them in the repo can lead to large and noisy commits
  # By default, only project.json and group.json files are stored in the repo (needed for the next syncs)
  GIT_IGNORE: "dependencies.json vulnerabilities.json ci-config.json *violations.json tmp public db/inventory.db protected_branches.json approvals.json approval_rules.json cicd_settings.json images-requested.json job_token_scope_allowlist.json"

  MERGE_REQUEST_DESCRIPTION: "Please review changes" # You can also use mentions here
  MERGE_REQUEST_TARGET: "${CI_DEFAULT_BRANCH}"

  # GITLAB_API_TOKEN is just here to document all variables.
  # NEVER add Personal Access Tokens or passwords to your CI configuration files.
  # Instead, set $GITLAB_API_TOKEN in your project CI settings as a protected variable
  # (https://docs.gitlab.com/ee/ci/variables/#protect-a-cicd-variable)
  GITLAB_API_TOKEN: ""
  # OR set these for Basic Auth:
  GITLAB_USERNAME: ""
  GITLAB_PASSWORD: ""

  # GIB variables
  LOGLEVEL: "info"
  GITLAB_API_BASEURL: "${CI_SERVER_URL}"
  # DATA_DIR_PATH is set to a working directly instead of the path in the repo
  # This is because sync might remove some directories, which would be preserved when restoring the artifact.
  DATA_DIR_PATH: "./working_data"
  # This is the original data dir path, which will be overridden
  DATA_DIR_PATH_IN_REPO: "./data"
  GIB_IMAGE_BASE: "registry.gitlab.com/gitlab-com/gl-security/product-security/gib"
  GIB_IMAGE_VERSION: ":latest"

  # Check SSL
  DISABLE_SSL_CHECK: "false"
  TESTSSL_SH_IMAGE: "drwetter/testssl.sh"
  SSL_REPORTS_PATH: "tmp/ssl"

  # SQLite database file
  INVENTORY_DB_ARCHIVE: db/inventory.db.7z

  # Compliance
  DISABLE_COMPLIANCE: "false"

  PROJECTS_POLICIES: "/usr/local/share/policies/projects/"
  PROJECTS_QUERY: "data.gitlab.projects.violation[violation]"

  SITES_POLICIES: "/usr/local/share/policies/sites/"
  SITES_QUERY: "data.gitlab.sites.violation[violation]"

  VULNERABILITIES_POLICIES: "/usr/local/share/policies/vulnerabilities/"
  VULNERABILITIES_QUERY: "data.gitlab.vulnerabilities.violation[violation]"

default:
  image:
    name: "${GIB_IMAGE_BASE}${GIB_IMAGE_VERSION}"
    entrypoint: [""]

include:
  - template: "Workflows/MergeRequest-Pipelines.gitlab-ci.yml"
  - template: "Jobs/Secret-Detection.latest.gitlab-ci.yml"

pre-flight:
  stage: .pre
  script:
    # Ignore json files
    - touch .gitignore
    - |
      for i in $GIT_IGNORE; do
        if ! grep -q "$i" .gitignore; then echo "$i" >> .gitignore; fi
      done
    # Avoid "fatal: detected dubious ownership in repository at ..." errors because the image is running as the user `gib`
    - git config --global --add safe.directory $PWD
    - |
      # Configure git lfs
      if [ ! -f .gitattributes ]; then
        git lfs install --force
      fi
    - git lfs track "$INVENTORY_DB_ARCHIVE"
    # Extract URLs for the ssl-check job (yq isn't available in the testssl image)
    - mkdir -p tmp
    - touch tmp/urls.txt
    - find $DATA_DIR_PATH_IN_REPO -name 'properties.yml' -exec yq eval --no-doc ".urls[]" \{\} \; > tmp/urls.txt
  artifacts:
    paths:
      - .gitignore
      - .gitattributes
      - tmp/urls.txt

.secret-analyzer:
  stage: validate
  needs: []

validate-data:
  stage: validate
  needs: []
  variables:
    DATA_DIR_PATH: "${DATA_DIR_PATH_IN_REPO}"
  script:
    - /usr/local/bin/gib validate
    # Calculate coverage
    - export properties=$(find $DATA_DIR_PATH -name 'properties.yml' -type f | wc -l)
    - export total_projects=$(find $DATA_DIR_PATH -name 'project.json' -type f | wc -l)
    - |
      echo "$properties $total_projects" | awk '{printf "Categorization: %d/%d projects (%.1f%% covered)\n", $1, $2, $1 *100 / $2}'
  coverage: '/\(\d+.\d+% covered\)/'

sync:
  stage: sync
  needs: ["validate-data"]
  script:
    - mv $DATA_DIR_PATH_IN_REPO $DATA_DIR_PATH
    - /usr/local/bin/gib sync
  artifacts:
    paths:
      - $DATA_DIR_PATH
  rules:
    - if: '$GITLAB_API_TOKEN == "" && $GITLAB_PASSWORD == ""'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

ssl-check:
  image:
    name: "$TESTSSL_SH_IMAGE"
    entrypoint: [""]
  stage: ssl-check
  needs: ["pre-flight"]
  script:
    - |
      mkdir -p "$SSL_REPORTS_PATH"
      while read url; do
        export filename=$(echo "$url" | sed -e 's/https:\/\///' -e 's/\/.*//')
        echo "Testing $url..."
        testssl.sh --htmlfile ${SSL_REPORTS_PATH}/${filename}.html --jsonfile ${SSL_REPORTS_PATH}/${filename}.json --overwrite --quiet $url 1>/dev/null &
      done < tmp/urls.txt
      wait
  artifacts:
    paths:
      - $SSL_REPORTS_PATH
  rules:
    - if: '$DISABLE_SSL_CHECK != "false"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

opa:
  stage: compliance
  script:
    - /usr/local/bin/gib compliance run
  artifacts:
    paths:
      - "${DATA_DIR_PATH}/**/violations.json"
      - "${SSL_REPORTS_PATH}/*violations.json"
  rules:
    - if: '$DISABLE_COMPLIANCE != "false"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'

update-db:
  stage: update
  script:
    - /usr/local/bin/gib update-db
  artifacts:
    paths:
      - $INVENTORY_DB_ARCHIVE
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'

create-issues:
  stage: create-issues
  script:
    - /usr/local/bin/gib compliance export
    - /usr/local/bin/gib compliance create-issues
  artifacts:
    paths:
      - tmp/violations.json
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'

pages:
  stage: report
  variables:
    HUGO_BASEURL: $CI_PAGES_URL
  script:
    - git config --global --add safe.directory $PWD
    - /usr/local/bin/gib generate-reports
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'

commit-changes:
  stage: commit
  needs: ["pre-flight", "sync", "update-db"]
  script:
    # restore DATA_DIR to the right place
    - rm -rf $DATA_DIR_PATH_IN_REPO
    - mv $DATA_DIR_PATH $DATA_DIR_PATH_IN_REPO
    # Avoid "fatal: detected dubious ownership in repository at ..." when running `git status`
    - git config --global --add safe.directory $PWD
    # Store only shorten versions of projects in the repo
    - find $DATA_DIR_PATH_IN_REPO -name project-short.json -exec bash -c 'mv -f $0 ${0/-short/}' \{\} \;
    # Exit if nothing to commit
    - if [ -z "$(git status --porcelain)" ]; then exit 0; fi
    # Local changes, create a MR to review
    - git config --global user.email "${GIT_USER_EMAIL}"
    - git config --global user.name "${GIT_USER_NAME}"
    - git config --global credential.username "${GIT_CREDENTIAL_USERNAME}"
    # $BRANCH can be overridden if necessary
    - if [ -z "${BRANCH}" ]; then export BRANCH="${CI_JOB_NAME// /-}-$CI_PIPELINE_IID"; fi
    - git checkout ${MERGE_REQUEST_TARGET}
    - git checkout -b ${BRANCH}
    # Disable Git's rename detection entirely
    - git config --local diff.renames 0
    # First commit only the deletions
    - git add --update
    - git commit -m "Remove deleted files from ${CI_JOB_ID} job" || true
    # Then commit the new files
    - git add --all
    - git commit -m "Add new files from ${CI_JOB_ID} job" || true
    - export PASSWORD=${GITLAB_PASSWORD:-GITLAB_API_TOKEN}
    - export PROJECT_URL="${CI_PROJECT_URL/https:\/\/$CI_SERVER_HOST/https://$GIT_CREDENTIAL_USERNAME:$PASSWORD@$CI_SERVER_HOST}.git"
    - git push -o merge_request.create -o merge_request.remove_source_branch -o merge_request.target="${MERGE_REQUEST_TARGET}" -o merge_request.description="${MERGE_REQUEST_DESCRIPTION}" ${PROJECT_URL} ${BRANCH}
  rules:
    - if: '$GITLAB_API_TOKEN == "" && $GITLAB_PASSWORD == ""'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
