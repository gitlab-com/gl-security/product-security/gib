package main

import (
	"bytes"
	_ "embed"
	"os/exec"

	"github.com/urfave/cli/v2"
)

//go:embed scripts/generate_reports.sh
var etlScript []byte

func generateReportsCmd(c *cli.Context) error {
	cmd := exec.Command("/bin/bash", "-s", "-l")
	cmd.Stdin = bytes.NewReader(etlScript)
	cmd.Stdout = c.App.Writer
	cmd.Stderr = c.App.Writer
	return cmd.Run()
}
