package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/gorilla/mux"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

func testServer(t *testing.T) (*mux.Router, *httptest.Server) {
	r := mux.NewRouter().UseEncodedPath()

	// Add a middleware to check header on all requests
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// by default, return only 1 page per query
			w.Header().Set("x-total-pages", "1")

			// The REST and GraphQL APIs use different authentication headers
			switch {
			case strings.HasPrefix(r.URL.Path, "/api/v4/groups/local-group/gitlab-local-token/"):
				if auth := r.Header.Get("Private-Token"); auth != "321fdsa" {
					t.Errorf("[%s] Authorization header: expected: '321fdsa', got: %s\n", r.URL, auth)
				}
			case strings.HasPrefix(r.URL.Path, "/api/v4/"): // REST API
				if auth := r.Header.Get("Private-Token"); auth != "asdf123" {
					t.Errorf("[%s] Authorization header: expected: 'asdf123' or '321fdsa', got: %s\n", r.URL, auth)
				}
			case strings.HasPrefix(r.URL.Path, "/api/graphql"): // GraphQL API
				if auth := r.Header.Get("Authorization"); auth != "Bearer asdf123" {
					t.Errorf("[%s] Authorization header: expected: Bearer asdf123, got: %s\n", r.URL, auth)
				}
			default: // Other URLs than the APIs should be an error
				t.Errorf("Unknown path: %s", r.URL.Path)
			}

			if r.Method == http.MethodPost {
				if ct := r.Header.Get("Content-Type"); ct != "application/json" {
					t.Errorf("Content-Type header: expected: application/json, got: %s\n", ct)
				}
			}
			// Call the next handler, which can be another middleware in the chain, or the final handler.
			next.ServeHTTP(w, r)
		})
	})

	projectsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "projects.json"))
	if err != nil {
		t.Fatal(err)
	}

	var projects []*gitlab.Project
	err = json.Unmarshal(projectsJSON, &projects)
	if err != nil {
		t.Fatal(err)
	}

	paginatedProjectsJSON := make([][]byte, 2)
	paginatedProjectsJSON[0], err = json.Marshal(projects[:len(projects)/2])
	if err != nil {
		t.Fatal(err)
	}
	paginatedProjectsJSON[1], err = json.Marshal(projects[len(projects)/2:])
	if err != nil {
		t.Fatal(err)
	}

	// simulate the group projects endpoint
	r.HandleFunc("/api/v4/groups/gitlab-org/projects", func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()

		if diff := cmp.Diff(q["per_page"], []string{"100"}); diff != "" {
			t.Errorf("per_page mismatch (-want, +got): %s", diff)
		}

		w.Header().Set("x-per-page", "100")
		w.Header().Set("x-total-pages", "2")

		// Simulate pagination
		page := q.Get("page")
		w.Header().Set("x-page", page)
		switch page {
		case "1":
			w.Header().Set("x-next-page", "2")
		case "2":
		default:
			t.Errorf("Unexpected page: %v", page)
		}

		pageInt, err := strconv.Atoi(page)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Fprintln(w, string(paginatedProjectsJSON[pageInt-1]))
	}).Methods(http.MethodGet)

	subgroupsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "subgroups.json"))
	if err != nil {
		t.Fatal(err)
	}

	var groups []*gitlab.Group
	err = json.Unmarshal(subgroupsJSON, &groups)
	if err != nil {
		t.Fatal(err)
	}

	paginatedSubgroupsJSON := make([][]byte, 2)
	paginatedSubgroupsJSON[0], err = json.Marshal(groups[:len(groups)/2])
	if err != nil {
		t.Fatal(err)
	}
	paginatedSubgroupsJSON[1], err = json.Marshal(groups[len(groups)/2:])
	if err != nil {
		t.Fatal(err)
	}

	// declarative-policy is a project with dependencies
	// It must be declared before the wildcard
	depsAPI, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "dependencies.json"))
	if err != nil {
		t.Fatal(err)
	}

	// simulate project dependencies endpoint
	r.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/dependencies", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, string(depsAPI))
	}).Methods(http.MethodGet)

	// simulate all projects dependencies endpoint, with an empty array
	r.HandleFunc("/api/v4/projects/{path_withnamespace}/dependencies", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "[]")
	}).Methods(http.MethodGet)

	vulnsAPI, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "vulnerabilities.json"))
	if err != nil {
		t.Fatal(err)
	}

	// simulate project vulnerabilities endpoint
	r.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/vulnerabilities", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, string(vulnsAPI))
	}).Methods(http.MethodGet)

	ciConfig, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "lint.json"))
	if err != nil {
		t.Fatal(err)
	}
	// simulate project CI-configuration endpoint
	r.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/ci/lint", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, string(ciConfig))
	}).Methods(http.MethodGet)

	pBranches, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "protected_branches.json"))
	if err != nil {
		t.Fatal(err)
	}
	// simulate project protected_branches endpoint
	r.HandleFunc("/api/v4/projects/{path_withnamespace}/protected_branches", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		if vars["path_withnamespace"] == "gitlab-org%2Fdeclarative-policy" {
			fmt.Fprintln(w, string(pBranches))
		} else {
			fmt.Fprintln(w, "[]")
		}
	}).Methods(http.MethodGet)

	approvals, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "approvals.json"))
	if err != nil {
		t.Fatal(err)
	}
	// simulate project approvals config endpoint
	r.HandleFunc("/api/v4/projects/{path_withnamespace}/approvals", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		if vars["path_withnamespace"] == "gitlab-org%2Fdeclarative-policy" {
			fmt.Fprintln(w, string(approvals))
		} else {
			fmt.Fprintln(w, "{}")
		}
	}).Methods(http.MethodGet)

	approvalRules, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "approval_rules.json"))
	if err != nil {
		t.Fatal(err)
	}
	// simulate project approval rules endpoint
	r.HandleFunc("/api/v4/projects/{path_withnamespace}/approval_rules", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		if vars["path_withnamespace"] == "gitlab-org%2Fdeclarative-policy" {
			fmt.Fprintln(w, string(approvalRules))
		} else {
			fmt.Fprintln(w, "[]")
		}
	}).Methods(http.MethodGet)

	jobTokenInboundAllowList, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "job_token_scope_allowlist.json"))
	if err != nil {
		t.Fatal(err)
	}
	// simulate project approval rules endpoint
	r.HandleFunc("/api/v4/projects/{path_withnamespace}/job_token_scope/allowlist", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		if vars["path_withnamespace"] == "gitlab-org%2Fdeclarative-policy" {
			fmt.Fprintln(w, string(jobTokenInboundAllowList))
		} else {
			fmt.Fprintln(w, "[]")
		}
	}).Methods(http.MethodGet)

	// simulate the group projects endpoint
	r.HandleFunc("/api/v4/groups/gitlab-org/subgroups", func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()

		if diff := cmp.Diff(q["per_page"], []string{"100"}); diff != "" {
			t.Errorf("per_page mismatch (-want, +got): %s", diff)
		}

		// Simulate pagination
		page := q.Get("page")
		w.Header().Set("x-page", page)
		switch page {
		case "1":
			w.Header().Set("x-next-page", "2")
		case "2":
		default:
			t.Errorf("Unexpected page: %v", page)
		}

		pageInt, err := strconv.Atoi(page)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Fprintln(w, string(paginatedSubgroupsJSON[pageInt-1]))
	}).Methods(http.MethodGet)

	fiveMinAppProjectsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "5-minute-production-app", "projects.json"))
	if err != nil {
		t.Fatal(err)
	}

	r.HandleFunc("/api/v4/groups/gitlab-org%2F5-minute-production-app/projects", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, string(fiveMinAppProjectsJSON))
	}).Methods(http.MethodGet)

	// catch urls like
	// /api/v4/groups/gitlab-org%2Farchitecture
	// /api/v4/groups/gitlab-org%2Farchitecture/subgroups
	// /api/v4/groups/gitlab-org%2Fci-cd/projects
	// [...]
	// And return empty arrays (stop walking in the data dir)
	r.HandleFunc("/api/v4/groups/gitlab-org%2F{subgroup:(?:5-minute-production-app|architecture|build|ci-cd)}/{action:(?:projects|subgroups)}", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "[]")
	}).Methods(http.MethodGet)

	r.HandleFunc("/api/graphql", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w,
			`{
				  "data": {
					"project": {
					  "ciCdSettings": {
						"inboundJobTokenScopeEnabled": true,
						"pushRepositoryForJobTokenAllowed": false
					  }
					}
				  }
				}`)
	})

	return r, httptest.NewServer(r)
}
