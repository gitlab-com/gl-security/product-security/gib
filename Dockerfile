ARG GO_VERSION=1.24
FROM golang:$GO_VERSION-alpine AS builder
RUN apk add --no-cache git

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /src

# Cache go dependencies
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . ./
RUN go build -v -ldflags="-s -w" -o /bin/gib

FROM mikefarah/yq:4 AS yq

FROM debian:stable-slim

ARG HUGO_VERSION
ENV HUGO_VERSION=${HUGO_VERSION:-0.144.2}

ARG SQLITE_UTILS_VERSION
ENV SQLITE_UTILS_VERSION=${SQLITE_UTILS_VERSION:-3.38}

ARG ARCH
ENV ARCH=${ARCH:-amd64}

ADD https://www.openpolicyagent.org/downloads/latest/opa_linux_${ARCH}_static /usr/local/bin/opa
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-${ARCH}.deb /tmp
RUN apt-get update && apt-get install -y --no-install-recommends \
  pipx jq ca-certificates git git-lfs sqlite3 p7zip-full && \
  dpkg -i /tmp/hugo_${HUGO_VERSION}_Linux-${ARCH}.deb && \
  chmod +x /usr/local/bin/opa && \
  groupadd --gid 1000 gib && \
  useradd --uid 1000 --gid gib --shell /bin/bash --create-home gib

USER gib

RUN pipx install sqlite-utils==${SQLITE_UTILS_VERSION} && \
  pipx ensurepath

COPY --from=builder /bin/gib /usr/local/bin/
COPY --from=yq /usr/bin/yq /usr/local/bin/
COPY policies/ /usr/local/share/policies/
COPY reports/theme/ /usr/local/share/themes/gitlab/


ENTRYPOINT ["/usr/local/bin/gib"]
