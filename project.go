package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-com/gl-security/product-security/gib/categories"
	"golang.org/x/sync/errgroup"

	log "github.com/sirupsen/logrus"
	gitlab "gitlab.com/gitlab-org/api/client-go"
	"sigs.k8s.io/yaml"
)

// Used to omit fields in marshaled structs
type omit *struct{}

// Project is a wrapper around *gitlab.Project
type Project struct {
	*gitlab.Project
	Dependencies             []*Dependency                 `json:"-"`
	Properties               *Properties                   `json:"-"`
	StoragePath              string                        `json:"-"` // Path of the under in the dataDir
	Vulnerabilities          []*Vulnerability              `json:"-"`
	CIConfig                 *gitlab.ProjectLintResult     `json:"-"`
	ProtectedBranches        []*gitlab.ProtectedBranch     `json:"-"`
	ApprovalConfig           *gitlab.ProjectApprovals      `json:"-"`
	ApprovalRules            []*gitlab.ProjectApprovalRule `json:"-"`
	CICDSettings             *CICDSettings                 `json:"-"`
	DockerImagesRequested    *DockerImageRequests          `json:"-"`
	JobTokenInboundAllowList []*gitlab.Project             `json:"-"`

	// Remove fields
	ForksCount                omit `json:"forks_count,omitempty"`
	StarCount                 omit `json:"star_count,omitempty"`
	OpenIssuesCount           omit `json:"open_issues_count,omitempty"`
	ContainerExpirationPolicy omit `json:"container_expiration_policy,omitempty"`
	RunnersToken              omit `json:"runners_token,omitempty"`
}

// ShortProject is a light representation of a Project with only essential
// fields to be stored in the repository.
// This short version is more stable than *gitlab.Project which gets new
// fields over time as GitLab evolves.
type ShortProject struct {
	ID                   int                    `json:"id"`
	Name                 string                 `json:"name"`
	WebURL               string                 `json:"web_url"`
	Description          string                 `json:"description"`
	DefaultBranch        string                 `json:"default_branch"`
	Visibility           gitlab.VisibilityValue `json:"visibility"`
	MarkedForDeletionAt  *gitlab.ISOTime        `json:"marked_for_deletion_at"`
	EmptyRepo            bool                   `json:"empty_repo"`
	Mirror               bool                   `json:"mirror"`
	ComplianceFrameworks []string               `json:"compliance_frameworks"`
}

// LoadProject loads a project from the project.json file in dataDir/path
func LoadProject(dataDir, path string) (*Project, error) {
	projectNamespace, err := filepath.Rel(dataDir, path)
	if err != nil {
		return nil, err
	}

	project := &Project{
		StoragePath: path,
		Project: &gitlab.Project{
			PathWithNamespace: projectNamespace,
		},
	}

	projectFile := filepath.Join(project.StoragePath, ProjectMetadataFile)
	if _, err := os.Stat(projectFile); os.IsNotExist(err) {
		// For the first sync, the project file isn't present
		// We just return the project skeleton that will be synced
		return project, nil
	}
	b, err := os.ReadFile(projectFile)
	if err != nil {
		log.Debugf("Project file %q not found", projectFile)
		return nil, err
	}
	if string(b) == "" {
		return nil, fmt.Errorf("%s file is empty", projectFile)
	}

	err = yaml.Unmarshal(b, &project)
	if err != nil {
		return nil, err
	}
	return project, nil
}

func (p Project) logger() *log.Entry {
	return log.WithFields(log.Fields{
		"project_name": p.Name,
		"project_path": p.PathWithNamespace,
	})
}

// Save creates or update all the files of the project p
func (p *Project) Save() error {
	logger := p.logger()

	// Create projectDir if doesn't exist
	if _, err := os.Stat(p.StoragePath); os.IsNotExist(err) {
		logger.Info("New project created")
		err := os.Mkdir(p.StoragePath, 0o700)
		if err != nil {
			return err
		}
	}

	logger.Info("Updating project")
	g := new(errgroup.Group)
	g.Go(p.storeMetadata)
	g.Go(p.storeShort)
	g.Go(p.storeDependencies)
	g.Go(p.storeVulnerabilities)
	g.Go(p.storeCIConfig)
	g.Go(p.storeProtectedBranches)
	g.Go(p.storeApprovalConfig)
	g.Go(p.storeApprovalRules)
	g.Go(p.storeCICDSettings)
	g.Go(p.StoreDockerImagesRequested)
	g.Go(p.StoreJobTokenInboundAllowList)

	return g.Wait()
}

func (p Project) storeMetadata() error {
	// save project metadata
	pjson, err := json.MarshalIndent(p, "", "   ")
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, ProjectMetadataFile), pjson, 0o600)
}

// storeShort stores a shorten version of the project metadata.
// This is the file that will be committed in the repo.
// The full version of the project (the `Project` struct) will be only stored in the DB.
func (p Project) storeShort() error {
	// Keep only essential fields
	shortProject := ShortProject{
		p.ID,
		p.Name,
		p.WebURL,
		p.Description,
		p.DefaultBranch,
		p.Visibility,
		p.MarkedForDeletionAt,
		p.EmptyRepo,
		p.Mirror,
		p.ComplianceFrameworks,
	}

	// save project metadata
	pjson, err := json.MarshalIndent(shortProject, "", "   ")
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, ShortProjectMetadataFile), pjson, 0o600)
}

func (p Project) storeDependencies() error {
	if len(p.Dependencies) == 0 {
		return nil // noop
	}

	deps, err := json.Marshal(p.Dependencies)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, DependenciesFile), deps, 0o600)
}

func (p Project) storeVulnerabilities() error {
	if len(p.Vulnerabilities) == 0 {
		return nil // noop
	}

	vulns, err := json.Marshal(p.Vulnerabilities)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, VulnerabilitiesFile), vulns, 0o600)
}

func (p Project) storeCIConfig() error {
	if p.CIConfig == nil {
		return nil // noop
	}

	ciConfig, err := json.Marshal(p.CIConfig)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, CIConfigFile), ciConfig, 0o600)
}

func (p Project) storeProtectedBranches() error {
	if p.ProtectedBranches == nil {
		return nil // noop
	}

	pb, err := json.Marshal(p.ProtectedBranches)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, ProtectedBranchesFile), pb, 0o600)
}

func (p Project) storeApprovalConfig() error {
	if p.ApprovalConfig == nil {
		return nil // noop
	}

	ac, err := json.Marshal(p.ApprovalConfig)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, ApprovalConfigFile), ac, 0o600)
}

func (p Project) storeApprovalRules() error {
	if len(p.ApprovalRules) == 0 {
		return nil // noop
	}

	ar, err := json.Marshal(p.ApprovalRules)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, ApprovalRulesFile), ar, 0o600)
}

func (p Project) storeCICDSettings() error {
	if p.CICDSettings == nil {
		return nil // noop
	}

	ccs, err := json.Marshal(p.CICDSettings)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, CICDSettingsFile), ccs, 0o600)
}

// StoreDockerImagesRequested stores images-requested.json files
func (p Project) StoreDockerImagesRequested() error {
	if p.DockerImagesRequested == nil {
		return nil // noop
	}

	jsonData, err := json.Marshal(p.DockerImagesRequested)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, DockerImageRequestsFile), jsonData, 0o600)
}

// JobTokenProject represents the minimal project information needed for job token scope
type JobTokenProject struct {
	ID                int    `json:"id"`
	PathWithNamespace string `json:"path_with_namespace"`
}

// StoreJobTokenInboundAllowList stores the list of projects in JobTokenInboundAllowList
func (p Project) StoreJobTokenInboundAllowList() error {
	if len(p.JobTokenInboundAllowList) == 0 {
		return nil // noop
	}

	// Convert gitlab.Project slice to JobTokenProject slice
	minimalProjects := make([]JobTokenProject, len(p.JobTokenInboundAllowList))
	for i, project := range p.JobTokenInboundAllowList {
		minimalProjects[i] = JobTokenProject{
			ID:                project.ID,
			PathWithNamespace: project.PathWithNamespace,
		}
	}

	jsonData, err := json.Marshal(minimalProjects)
	if err != nil {
		return err
	}

	return os.WriteFile(filepath.Join(p.StoragePath, JobTokenScopeAllowlistFile), jsonData, 0o600)
}

// LoadProperties reads the project's properties.yml file, and loads it into the current project
func (p *Project) LoadProperties() error {
	p.Properties = &Properties{}
	propFile := filepath.Join(p.StoragePath, PropertiesFile)
	if _, err := os.Stat(propFile); os.IsNotExist(err) {
		return nil
	}
	b, err := os.ReadFile(propFile)
	if err != nil {
		return err
	}
	if string(b) == "" {
		return fmt.Errorf("Properties file %q is empty", propFile)
	}
	return yaml.Unmarshal(b, &p.Properties)
}

// Sync sends a request to the GitLab API to get the project metadata
func (p *Project) Sync(git *GitLabClient) error {
	hasIgnoreFile, err := p.hasIgnoreFile()
	if err != nil {
		return err
	}
	if hasIgnoreFile {
		p.logger().Info("ignore file found, not syncing")
		// Make sure we don't leave project.json files behind.
		// If the project has been synced previously, there's a project.json file
		// in its folder. This file won't be synced and is a shorten version.
		// When projects are imported in the DB, some fields will be empty.
		if _, err := os.Stat(filepath.Join(p.StoragePath, ProjectMetadataFile)); err == nil {
			p.logger().Info("cleaning project.json file since project is ignored")
			return os.Remove(filepath.Join(p.StoragePath, ProjectMetadataFile))
		}
		return nil
	}

	if p.ID == 0 {
		p.logger().Info("Syncing project")
		project, _, err := git.Projects.GetProject(p.PathWithNamespace, &gitlab.GetProjectOptions{}, nil)
		if err != nil {
			return err
		}
		p.Project = project
	}

	if err := p.LoadProperties(); err != nil {
		return err
	}

	if p.EmptyRepo ||
		p.RepositoryAccessLevel == "disabled" ||
		git.DisableAllDownloads {
		return p.Save()
	}

	g := new(errgroup.Group)
	ch := make(chan interface{}, 100)

	if p.shouldDownloadDependencies(git) {
		g.Go(func() error { return p.downloadDependencies(git, ch) })
	}

	if p.shouldDownloadVulnerabilities(git) {
		g.Go(func() error { return p.downloadVulnerabilities(git, ch) })
	}

	if p.shouldDownloadCIConfigs(git) {
		g.Go(func() error { return p.downloadCIConfig(git, ch) })
	}

	if p.shouldDownloadProtectedBranches(git) {
		g.Go(func() error { return p.downloadProtectedBranches(git, ch) })
	}

	if p.shouldDownloadApprovalRules(git) {
		g.Go(func() error { return p.downloadApprovalConfiguration(git, ch) })
		g.Go(func() error { return p.downloadApprovalRules(git, ch) })
	}

	if p.shouldDownloadCICDSettings(git) {
		g.Go(func() error { return p.downloadCICDSettings(git, ch) })
	}

	if p.shouldDownloadJobTokenInboundAllowList(git) {
		g.Go(func() error { return p.downloadJobTokenInboundAllowList(git, ch) })
	}

	go func() {
		g.Wait() // nolint
		close(ch)
	}()
	vulnerabilities := []*Vulnerability{}
	dependencies := []*Dependency{}
	ciConfig := &gitlab.ProjectLintResult{}
	protectedBranches := []*gitlab.ProtectedBranch{}
	approvalConfig := &gitlab.ProjectApprovals{}
	approvalRules := []*gitlab.ProjectApprovalRule{}
	ciCdSettings := &CICDSettings{}
	jobTokenInboundAllowList := []*gitlab.Project{}

	for result := range ch {
		switch t := result.(type) {
		case []*Vulnerability:
			vulnerabilities = append(vulnerabilities, t...)
		case []*Dependency:
			dependencies = append(dependencies, t...)
		case *gitlab.ProjectLintResult:
			ciConfig = t
		case []*gitlab.ProtectedBranch:
			protectedBranches = append(protectedBranches, t...)
		case *gitlab.ProjectApprovals:
			approvalConfig = t
		case []*gitlab.ProjectApprovalRule:
			approvalRules = append(approvalRules, t...)
		case *CICDSettings:
			ciCdSettings = t
		case []*gitlab.Project:
			jobTokenInboundAllowList = append(jobTokenInboundAllowList, t...)
		default:
			return fmt.Errorf("unknown type downloaded: %T", t)
		}
	}

	if err := g.Wait(); err != nil {
		return err
	}
	if p.shouldDownloadDependencies(git) {
		p.Dependencies = dependencies
		p.logger().Infof("Downloaded %d dependencies", len(p.Dependencies))
	}

	if err := p.mergeDependencies(git); err != nil {
		return err
	}

	if p.shouldDownloadVulnerabilities(git) {
		p.Vulnerabilities = vulnerabilities
		p.logger().Infof("Downloaded %d vulnerabilities", len(p.Vulnerabilities))
	}

	if p.shouldDownloadCIConfigs(git) {
		p.CIConfig = ciConfig
	}

	if p.shouldDownloadProtectedBranches(git) {
		p.ProtectedBranches = protectedBranches
		p.logger().Infof("Downloaded %d protected branches", len(p.ProtectedBranches))
	}

	if p.shouldDownloadApprovalRules(git) {
		p.ApprovalConfig = approvalConfig
		p.ApprovalRules = approvalRules
		p.logger().Infof("Downloaded %d approval rules", len(p.ApprovalRules))
	}

	if p.shouldDownloadCICDSettings(git) {
		p.CICDSettings = ciCdSettings
		p.logger().Info("Downloaded CI/CD settings")
	}

	if p.shouldDownloadJobTokenInboundAllowList(git) {
		p.JobTokenInboundAllowList = jobTokenInboundAllowList
		p.logger().Info("Downloaded JobTokenInboundAllowList")
	}

	return p.Save()
}

// HasCategory returns true if the project has the category in its properties
func (p Project) HasCategory(category categories.Category) bool {
	for _, cat := range p.Properties.Categories {
		if cat == category {
			return true
		}
	}
	return false
}

func (p Project) downloadDependencies(git *GitLabClient, ch chan interface{}) error {
	p.logger().Info("Downloading dependencies")

	downloader := func(page int) ([]*Dependency, *gitlab.Response, error) {
		var dependencies []*Dependency
		listOpts := gitlab.ListOptions{
			PerPage: 100,
			Page:    page,
		}

		u := fmt.Sprintf("projects/%s/dependencies",
			url.PathEscape(p.PathWithNamespace))

		req, err := git.NewRequest(http.MethodGet, u, listOpts, nil)
		if err != nil {
			return nil, nil, err
		}

		response, err := git.Do(req, &dependencies)
		return dependencies, response, err
	}

	return downloadPaginatedItems(downloader, p.logger, ch)
}

func (p Project) downloadVulnerabilities(git *GitLabClient, ch chan interface{}) error {
	p.logger().Info("Downloading vulnerabilities")

	downloader := func(page int) ([]*Vulnerability, *gitlab.Response, error) {
		var vulnerabilities []*Vulnerability
		listOpts := gitlab.ListOptions{
			PerPage: 100,
			Page:    page,
		}

		u := fmt.Sprintf("projects/%s/vulnerabilities",
			strings.ReplaceAll(url.PathEscape(p.PathWithNamespace), ".", "%2E"))

		req, err := git.NewRequest(http.MethodGet, u, listOpts, nil)
		if err != nil {
			return nil, nil, err
		}

		response, err := git.Do(req, &vulnerabilities)
		return vulnerabilities, response, err
	}

	return downloadPaginatedItems(downloader, p.logger, ch)
}

func (p Project) downloadCIConfig(git *GitLabClient, ch chan interface{}) error {
	p.logger().Info("Downloading CI/CD configuration")
	CIConfig, _, err := git.Validate.ProjectLint(p.PathWithNamespace, &gitlab.ProjectLintOptions{})
	ch <- CIConfig
	p.logger().Info("Downloaded CI/CD Configuration")
	return err
}

func (p Project) downloadProtectedBranches(git *GitLabClient, ch chan interface{}) error {
	p.logger().Info("Downloading protected branches")

	downloader := func(page int) ([]*gitlab.ProtectedBranch, *gitlab.Response, error) {
		return git.ProtectedBranches.ListProtectedBranches(
			p.PathWithNamespace,
			&gitlab.ListProtectedBranchesOptions{
				ListOptions: gitlab.ListOptions{
					PerPage: 100,
					Page:    page,
				},
				Search: nil,
			},
		)
	}

	return downloadPaginatedItems(downloader, p.logger, ch)
}

func (p Project) downloadApprovalConfiguration(git *GitLabClient, ch chan interface{}) error {
	p.logger().Info("Downloading approval configuration")

	approvalConfig, _, err := git.Projects.GetApprovalConfiguration(p.PathWithNamespace)
	ch <- approvalConfig
	p.logger().Info("Downloaded approval configuration")
	return err
}

func (p Project) downloadApprovalRules(git *GitLabClient, ch chan interface{}) error {
	p.logger().Info("Downloading approval rules")

	downloader := func(page int) ([]*gitlab.ProjectApprovalRule, *gitlab.Response, error) {
		return git.Projects.GetProjectApprovalRules(
			p.PathWithNamespace,
			&gitlab.GetProjectApprovalRulesListsOptions{
				Page: page,
			},
		)
	}

	return downloadPaginatedItems(downloader, p.logger, ch)
}

type ciCdSettingsResponse struct {
	Data struct {
		Project struct {
			CICDSettings CICDSettings `json:"ciCdSettings"`
		} `json:"project"`
	} `json:"data"`
}

// CICDSettings represents project CI/CD Settings: https://docs.gitlab.com/ee/api/graphql/reference/#projectcicdsetting
type CICDSettings struct {
	InboundJobTokenScopeEnabled      bool `json:"inboundJobTokenScopeEnabled"`
	PushRepositoryForJobTokenAllowed bool `json:"pushRepositoryForJobTokenAllowed"`
}

func (p Project) downloadCICDSettings(git *GitLabClient, ch chan interface{}) error {
	p.logger().Info("Downloading CI/CD settings")

	query := `
		query GetInboundJobTokenScopeEnabled($projectName: ID!) {
			project(fullPath: $projectName) {
				ciCdSettings {
					inboundJobTokenScopeEnabled
					pushRepositoryForJobTokenAllowed
				}
			}
		}`

	var responseBody ciCdSettingsResponse
	err := git.GraphQLClient.ExecuteQuery(query, map[string]interface{}{"projectName": p.PathWithNamespace}, &responseBody)
	if err != nil {
		return err
	}

	ciCdSettings := &responseBody.Data.Project.CICDSettings

	ch <- ciCdSettings

	return err
}

func (p Project) downloadJobTokenInboundAllowList(git *GitLabClient, ch chan interface{}) error {
	downloader := func(page int) ([]*gitlab.Project, *gitlab.Response, error) {
		return git.JobTokenScope.GetProjectJobTokenInboundAllowList(
			p.PathWithNamespace,
			&gitlab.GetJobTokenInboundAllowListOptions{
				ListOptions: gitlab.ListOptions{
					PerPage: 100,
					Page:    page,
				},
			},
		)
	}

	return downloadPaginatedItems(downloader, p.logger, ch)
}

// mergeDependencies merges dependencies from Properties into the project dependencies
// See https://gitlab.com/gitlab-com/gl-security/product-security/gib/-/issues/27
func (p *Project) mergeDependencies(git *GitLabClient) error {
	if len(p.Properties.Dependencies) == 0 {
		return nil
	}

	p.logger().Infof("Merging %d dependencies from Properties", len(p.Properties.Dependencies))
	for _, d := range p.Properties.Dependencies {
		if err := d.FetchVersion(git, p.PathWithNamespace, p.DefaultBranch); err != nil {
			return err
		}
		p.Dependencies = append(p.Dependencies, &d)
	}
	return nil
}

func (p Project) shouldDownloadDependencies(git *GitLabClient) bool {
	return !git.DisableDependenciesDownload
}

func (p Project) shouldDownloadVulnerabilities(git *GitLabClient) bool {
	return !git.DisableVulnerabilitiesDownload && p.isCritical()
}

func (p Project) shouldDownloadCIConfigs(git *GitLabClient) bool {
	return !git.DisableCIConfigDownload && p.isCritical()
}

func (p Project) shouldDownloadProtectedBranches(git *GitLabClient) bool {
	return !git.DisableProtectedBranchesDownload
}

func (p Project) shouldDownloadApprovalRules(git *GitLabClient) bool {
	return !git.DisableApprovalRulesDownload
}

func (p Project) shouldDownloadCICDSettings(git *GitLabClient) bool {
	return !git.DisableCICDSettingsDownload
}

func (p Project) shouldDownloadJobTokenInboundAllowList(git *GitLabClient) bool {
	return !git.DisableJobTokenInboundAllowList
}

func (p Project) hasIgnoreFile() (bool, error) {
	if _, err := os.Stat(p.StoragePath); os.IsNotExist(err) {
		return false, nil
	}
	files, err := os.ReadDir(p.StoragePath)
	return hasIgnoreFile(files), err
}

func (p Project) isCritical() bool {
	return p.HasCategory(categories.Product) || p.HasCategory(categories.Library) || p.HasCategory(categories.RedData)
}
