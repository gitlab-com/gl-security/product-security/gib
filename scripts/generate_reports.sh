#!/bin/bash
# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

if [ -z "$GITLAB_API_TOKEN" ]; then
  echo "WARNING: GITLAB_API_TOKEN is not set, reports generation might fail"
fi

DATADIR=${DATA_DIR_PATH:=./data}
INVENTORY_DB=$DATADIR/../db/inventory.db
TEMP_FOLDER=tmp/
INVENTORY_SITE=inventory
CI_DEFAULT_BRANCH=${CI_DEFAULT_BRANCH:=main}

function generate_projects_pages() {
  # Some tables might not have been created, hence the test for their existance
  local tables=$(sqlite-utils tables ../$INVENTORY_DB --csv --no-headers)
  local request

  # Extract projects from the local DB to create Hugo pages (`content` directory)

  if $(echo $tables | grep -q "urls"); then
    request=$(
      cat <<-END
				    select projects.id, projects.path_with_namespace, json_group_array(distinct categories.category) as categories, json_group_array(distinct(urls.url)) as urls
				    from projects
				      left outer join categories on categories.project_id = projects.id
				      left outer join urls on urls.project_id = projects.id
				    group by categories.project_id, projects.path_with_namespace
			END
    )
  else
    request=$(
      cat <<-END
				    select projects.id, projects.path_with_namespace, json_group_array(categories.category) as categories
				    from projects
				      left outer join categories on categories.project_id = projects.id
				    group by categories.project_id, projects.path_with_namespace
			END
    )
  fi

  sqlite-utils ../$INVENTORY_DB "$request" --nl --json-cols |
    while IFS= read -r line; do
      local path_with_namespace=$(echo $line | jq .path_with_namespace -r)
      local dir=${INVENTORY_SITE}/content/projects/$(dirname $path_with_namespace)
      local project=$(basename $path_with_namespace)
      mkdir -p $dir
      echo "Creating page $dir/$path_with_namespace"
      echo $line | jq 'del(.categories,.urls | select(. == [null]))' >${dir}/${project}.md
    done
}

function normalize_docker_path() {
  local image_path=$1

  # If the path doesn't contain a slash, it's a Docker Hub official image
  if [[ $image_path != *"/"* ]]; then
    image_path="docker.io/library/$image_path"
  # If the path has one slash but no dot prefix, it's a Docker Hub user image
  elif [[ $image_path != *"."* ]] && [[ $(echo "$image_path" | tr -cd '/' | wc -c) -eq 1 ]]; then
    image_path="docker.io/$image_path"
  fi

  echo "$image_path"
}

function image_requested_page_path() {
  local image_name=$1

  # Normalize the path to include docker.io if needed
  local image_path=$(normalize_docker_path "$image_name")

  # Handle case where there might be a port number (host:port)
  # If there's still a colon in the path, it must be a port number
  if [[ $image_path == *":"* ]]; then
    # Replace the colon with a hyphen for the port number
    image_path=$(echo "$image_path" | sed 's/\([^:]*\):\([^/]*\)/\1-\2/')
  fi

  echo "$image_path.md"
}

function generate_docker_images_requested_pages() {
  # Some tables might not have been created, hence the test for their existance
  local tables=$(sqlite-utils tables ../$INVENTORY_DB --csv --no-headers)

  if ! $(echo $tables | grep -q "images_requested"); then
    echo "Table 'images_requested' not found, skipping"
    return
  fi

  # This query groups images by name without tag or digest, and associate them with a json map where tags are keys and values are images_requested rows,
  # For example, with 2 rows in the `images_requested` table for `alpine:3`, the result with look like:
  # {
  # "3": [
  #     {
  #         "digest": "sha256:1e42bbe2508154c9126d48c2b8a75420c3544343bf86fd041fb7527e017a4b4a",
  #         "job_name": "check_files",
  #         "last_job_id": 8559045480,
  #         "last_used_at": "2024-12-05T15:37:23.008Z",
  #         "project": "components/sbom/generator"
  #     },
  #     {
  #         "digest": "sha256:1e42bbe2508154c9126d48c2b8a75420c3544343bf86fd041fb7527e017a4b4a",
  #         "job_name": "publish package",
  #         "last_job_id": 8559045483,
  #         "last_used_at": "2024-12-05T15:37:23.02Z",
  #         "project": "components/sbom/generator"
  #     }
  # }
  local request=$(
    cat <<-SQL
			WITH parsed_images AS (
			    SELECT
			        CASE
			            -- Handle image with both tag and digest
			            WHEN instr(image_name, ':') > 0 AND instr(image_name, '@sha256:') > 0 AND instr(image_name, ':') < instr(image_name, '@sha256:') THEN
			                rtrim(substr(image_name, 1, instr(image_name, ':') - 1), ':')
			            -- Handle image with just a digest
			            WHEN instr(image_name, '@sha256:') > 0 THEN
			                rtrim(substr(image_name, 1, instr(image_name, '@sha256:') - 1), ':')
			            -- Handle image with just tag
			            WHEN instr(image_name, ':') > 0 AND instr(substr(image_name, instr(image_name, '/') + 1), ':') > 0 THEN
			                rtrim(substr(
			                    image_name,
			                    1,
			                    length(image_name) - length(substr(image_name, instr(substr(image_name, instr(image_name, '/') + 1), ':') + instr(image_name, '/') + 1))
			                ), ':')
			            WHEN instr(image_name, ':') > 0 THEN
			                rtrim(substr(image_name, 1, instr(image_name, ':') - 1), ':')
			            ELSE
			                rtrim(image_name, ':')
			        END AS parsed_image_name,
			        CASE
			            -- Handle image with both tag and digest
			            WHEN instr(image_name, ':') > 0 AND instr(image_name, '@sha256:') > 0 AND instr(image_name, ':') < instr(image_name, '@sha256:') THEN
			                substr(
			                    image_name,
			                    instr(image_name, ':') + 1,
			                    instr(image_name, '@sha256:') - instr(image_name, ':') - 1
			                )
			            -- Handle image with just a digest
			            WHEN instr(image_name, '@sha256:') > 0 THEN
			                'latest'
			            -- Handle image with just tag
			            WHEN instr(image_name, ':') > 0 AND instr(substr(image_name, instr(image_name, '/') + 1), ':') > 0 THEN
			                substr(
			                    image_name,
			                    instr(substr(image_name, instr(image_name, '/') + 1), ':') + instr(image_name, '/') + 1
			                )
			            WHEN instr(image_name, ':') > 0 THEN
			                substr(image_name, instr(image_name, ':') + 1)
			            ELSE
			                'latest'
			        END AS image_tag,
			       digest,
			        job_name,
			        last_job_id,
			        last_used_at,
			        projects.path_with_namespace as project,
					(instr(image_name, '@sha256:') > 0) digest_used
			    FROM images_requested
			    join projects on projects.id = images_requested.project_id
			),
			tag_data AS (
			   SELECT
			    parsed_image_name as image_name,
			    image_tag,
			    group_concat(
			        json_object(
			            'job_name', job_name,
			            'last_job_id', last_job_id,
			            'last_used_at', last_used_at,
			            'digest', digest,
			            'digest_used', json(CASE WHEN digest_used = 1 THEN 'true' ELSE 'false' END),
			            'project', project
			        )
			    ) as json_array
			    FROM parsed_images
			    GROUP BY parsed_image_name, image_tag
			)
			SELECT
			    image_name,
			    '{' || group_concat(
			        '"' || image_tag || '":[' || json_array || ']',
			        ','
			    ) || '}' as tags
			FROM tag_data
			GROUP BY image_name;
		SQL
  )

  # For each image, a Hugo page is created by generated a markdown file with the name of the image
  # Ports are translated in hyphens, so dev.gitlab.org:5000/someimage becomes dev.gitlab.org-5000/someimage.
  # The files are stored in a folder corresponding to the url, and they contain all the data related to the tags of that image.
  # In Hugo, front matter data can be json directly.
  local dir=${INVENTORY_SITE}/content/docker/images_requested/
  sqlite-utils ../$INVENTORY_DB "$request" --nl --json-cols |
    while IFS= read -r line; do
      local image_name=$(echo "$line" | jq '.image_name' -r)
      local page=$(image_requested_page_path "$image_name")
      echo "Creating page ${dir}${page}"
      mkdir -p "$dir/$(dirname $page)"
      echo "$line" >"$dir$page"
    done
}

if [ -f "${INVENTORY_DB}.7z" ]; then
  echo "Extracting inventory DB"
  7z x -y ${INVENTORY_DB}.7z
fi

echo "Configuring your inventory website"
# the temporary folder is an artifact, and should have been created already
mkdir -p $TEMP_FOLDER
cd $TEMP_FOLDER

if [ -d "${INVENTORY_SITE}" ]; then
  echo "${INVENTORY_SITE} already exists, skipping creation"
else
  hugo new site inventory
  ln -sf /usr/local/share/themes/gitlab ${INVENTORY_SITE}/themes/
  cp -r ${INVENTORY_SITE}/themes/gitlab/config/ ${INVENTORY_SITE}/
  cp ${INVENTORY_SITE}/themes/gitlab/config/_default/config.toml ${INVENTORY_SITE}/

  echo "Adding SSL reports to static/ and data/urls/"
  mkdir ${INVENTORY_SITE}/data/urls/
  if [ -d ssl ]; then
    cp -r ssl ${INVENTORY_SITE}/static/
    mv ${INVENTORY_SITE}/static/ssl/*.json ${INVENTORY_SITE}/data/urls || true
  fi

  # We use the data dir as an input for Hugo data directly.
  # Hugo doesn't allow symlinks starting 0.123 (with https://github.com/gohugoio/hugo/issues/11556).
  # Folders outside of the site root must be mounted instead (it's more efficient than copying the whole data dir)
  #
  # See https://gohugo.io/content-management/data-sources/
  # and https://gohugo.io/hugo-modules/configuration/#module-configuration-mounts
  echo "Mounting inventory data to your site data"
  cat >>${INVENTORY_SITE}/hugo.toml <<-END

		  [module]
		  [[module.mounts]]
		  source = 'data'
		  target = 'data'
		  [[module.mounts]]
		  source = '../../${DATADIR}'
		  target = 'data/projects'
	END

  echo "Copying violations.json to data"
  cp violations.json ${INVENTORY_SITE}/data/
fi

function generate_projects_metrics {
  local projects_metrics=${INVENTORY_SITE}/data/projects_metrics.json
  local request
  sqlite-utils ../$INVENTORY_DB "select sum(1) as total_number_of_projects from projects" --nl >$projects_metrics

  git diff --name-only --diff-filter=A "$(git rev-list -1 --before 7.days.ago origin/$CI_DEFAULT_BRANCH)" origin/$CI_DEFAULT_BRANCH | grep -e 'project.json$' | sed -e 's/^data//g' -e 's/\/project.json//g' | jq -ncMR '{"added": [inputs]}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics
  git diff --name-only --diff-filter=D "$(git rev-list -1 --before 7.days.ago origin/$CI_DEFAULT_BRANCH)" origin/$CI_DEFAULT_BRANCH | grep -e 'project.json$' | sed -e 's/^data//g' -e 's/\/project.json//g' | jq -ncMR '{"deleted": [inputs]}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics
  git diff --name-only --diff-filter=MRT "$(git rev-list -1 --before 7.days.ago origin/$CI_DEFAULT_BRANCH)" origin/$CI_DEFAULT_BRANCH | grep -e 'project.json$' | sed -e 's/^data//g' -e 's/\/project.json//g' | jq -ncMR '{"updated": [inputs]}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics
  # Count ignored groups
  find ../$DATADIR -name ignore -type f | wc -l | awk '{print $1}' | jq -ncMR '{"ignored": inputs}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics

  # cleanup
  rm -f $projects_metrics.tmp

  # Some tables might not have been created, hence the test for their existance
  local tables=$(sqlite-utils tables ../$INVENTORY_DB --csv --no-headers)

  if $(echo $tables | grep -q "vulnerabilities"); then
    echo "Generating Vulnerabilities data"
    mkdir -p ${INVENTORY_SITE}/data/vulnerabilities
    request=$(
      cat <<-END
				  select severity,
				          sum(case when created_at <  DATE('now', '-30 day') then 1 else 0 end) as over_30_days,
				          sum(case when created_at <  DATE('now', '-60 day') then 1 else 0 end) as over_60_days,
				          sum(case when created_at <  DATE('now', '-90 day') then 1 else 0 end) as over_90_days,
				          sum(case when created_at <  DATE('now', '-120 day') then 1 else 0 end) as over_120_days,
				          sum(case when created_at <  DATE('now', '-1 year') then 1 else 0 end) as over_1_year,
				          sum(1) all_time
				  from vulnerabilities
				  where vulnerabilities.state in ('detected', 'confirmed')
				  group by 1
				  order by case
				          when severity='critical' then 1
				          when severity='high' then 2
				          when severity='medium' then 3
				          when severity='low' then 4
				          when severity='info' then 5
				          when severity='unknown' then 6
				  end
			END
    )
    sqlite-utils "../$INVENTORY_DB" "$request" >${INVENTORY_SITE}/data/vulnerabilities/aging.json

    request=$(
      cat <<-END
				   select projects.path_with_namespace,
				    cast( avg(case when  vulnerabilities.severity in ('critical','high','unknown') then julianday('now') - julianday(vulnerabilities.created_at) end) as integer) as avg_age_of_vulnerabilities_crit,
				    sum(case when vulnerabilities.severity in ('critical','high','unknown') then 1 end) as number_of_vulnerabilities_crit,
				    cast( avg(julianday('now') - julianday(vulnerabilities.created_at)) as integer) as avg_age_of_vulnerabilities,
				    sum(1) as number_of_vulnerabilities
				   from vulnerabilities
				   join projects on vulnerabilities.project_id = projects.id
				   where vulnerabilities.state in ('detected','confirmed')
				   group by 1
				   order by 2 desc
				   limit 10
			END
    )
    sqlite-utils "../$INVENTORY_DB" "$request" >${INVENTORY_SITE}/data/vulnerabilities/oldest_average.json

    request=$(
      cat <<-END
				  select
				    projects.path_with_namespace,
				    coalesce(sum(case when vulnerabilities.state = 'dismissed' then 1 end),0) as FP,
				    coalesce(sum(case when vulnerabilities.state in ('confirmed', 'resolved') then 1 end), 0) as TP,
				    sum(1) as total,
				    round(coalesce(sum(case when vulnerabilities.state = 'dismissed' then 1 end)/cast(sum(1) as float),0)*100, 2) as FPR,
				    round(coalesce(sum(case when vulnerabilities.state in ('confirmed', 'resolved') and confirmed_at is not null then 1 end)/cast(sum(1) as float),0)*100,2) as TPR
				  from vulnerabilities
				    join projects on vulnerabilities.project_id = projects.id
				  where vulnerabilities.state in ('dismissed', 'confirmed', 'resolved')
				  group by 1
				  order by fpr desc, total desc
				  limit 10;
			END
    )
    sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/worse_false_positive_rates.json

    request=$(
      cat <<-END
				  select
				    projects.path_with_namespace,
				    coalesce(sum(case when vulnerabilities.state = 'dismissed' then 1 end),0) as FP,
				    coalesce(sum(case when vulnerabilities.state in ('confirmed', 'resolved') then 1 end), 0) as TP,
				    sum(1) as total,
				    round(coalesce(sum(case when vulnerabilities.state = 'dismissed' then 1 end)/cast(sum(1) as float),0)*100, 2) as FPR,
				    round(coalesce(sum(case when vulnerabilities.state in ('confirmed', 'resolved') and confirmed_at is not null then 1 end)/cast(sum(1) as float),0)*100,2) as TPR
				  from vulnerabilities
				    join projects on vulnerabilities.project_id = projects.id
				  where vulnerabilities.state in ('dismissed', 'confirmed', 'resolved')
				  group by 1
				  order by TPR desc, total desc
				  limit 10;
			END
    )
    sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/best_true_positive_rates.json

    request=$(
      cat <<-END
				  select
				    projects.path_with_namespace,

				    coalesce(sum(case when vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='confirmed' and severity = 'critical' then 1 end), 0) confirmed_critical,
				    coalesce(sum(case when vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='confirmed' and severity = 'high' then 1 end), 0) confirmed_high,
				    coalesce(sum(case when vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='confirmed' and severity = 'unknown' then 1 end), 0) confirmed_unknown,
				    coalesce(sum(case when vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='confirmed' and severity = 'medium' then 1 end), 0) confirmed_medium,
				    coalesce(sum(case when vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='confirmed' and severity = 'low' then 1 end), 0) confirmed_low,
				    coalesce(sum(case when vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='confirmed' and severity = 'info' then 1 end), 0) confirmed_info,
				    coalesce(sum(case when vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='confirmed' then 1 end), 0) total_confirmed,

				    coalesce(sum(case when vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='dismissed' and severity = 'critical' then 1 end), 0) dismissed_critical,
				    coalesce(sum(case when vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='dismissed' and severity = 'high' then 1 end), 0) dismissed_high,
				    coalesce(sum(case when vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='dismissed' and severity = 'unknown' then 1 end), 0) dismissed_unknown,
				    coalesce(sum(case when vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='dismissed' and severity = 'medium' then 1 end), 0) dismissed_medium,
				    coalesce(sum(case when vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='dismissed' and severity = 'low' then 1 end), 0) dismissed_low,
				    coalesce(sum(case when vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='dismissed' and severity = 'info' then 1 end), 0) dismissed_info,
				    coalesce(sum(case when vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='dismissed' then 1 end), 0) total_dismissed,

				    coalesce(sum(case when vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='resolved' and severity = 'critical' then 1 end), 0) resolved_critical,
				    coalesce(sum(case when vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='resolved' and severity = 'high' then 1 end), 0) resolved_high,
				    coalesce(sum(case when vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='resolved' and severity = 'unknown' then 1 end), 0) resolved_unknown,
				    coalesce(sum(case when vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='resolved' and severity = 'medium' then 1 end), 0) resolved_medium,
				    coalesce(sum(case when vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='resolved' and severity = 'low' then 1 end), 0) resolved_low,
				    coalesce(sum(case when vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='resolved' and severity = 'info' then 1 end), 0) resolved_info,
				    coalesce(sum(case when vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days') and vulnerabilities.state='resolved' then 1 end),0) total_resolved

				  from vulnerabilities
				    join projects on vulnerabilities.project_id = projects.id
				  where vulnerabilities.state in ('dismissed', 'resolved', 'confirmed')
				    and (vulnerabilities.dismissed_at >= date('now', 'weekday 0', '-37 days')
				    or vulnerabilities.resolved_at >= date('now', 'weekday 0', '-37 days')
				    or vulnerabilities.updated_at >= date('now', 'weekday 0', '-37 days')
				    )
				  group by 1
				  order by total_dismissed DESC , total_resolved DESC
			END
    )
    sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/triage_ratios.json

    request=$(
      cat <<-END
				    with dates as (
				      select datetime('now', '-6 months') as reportWeek
				      union all
				      select datetime(reportWeek, '+7 days')
				      from dates
				      where reportWeek < datetime('now', 'weekday 0', '-7 days')
				     ), weekly as (
				    select strftime('%W', case vulnerabilities.state when 'dismissed' then dismissed_at when 'confirmed' then updated_at when 'resolved' then resolved_at end) WeekNumber,
				      sum(case when severity = 'critical' then 1 else 0 end) critical,
				      sum(case when severity = 'critical' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_critical,
				      sum(case when severity = 'critical' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_critical,
				      sum(case when severity = 'critical' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_critical,
				      sum(case when severity = 'high' then 1 else 0 end) high,
				      sum(case when severity = 'high' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_high,
				      sum(case when severity = 'high' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_high,
				      sum(case when severity = 'high' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_high,
				      sum(case when severity = 'unknown' then 1 else 0 end) unknown,
				      sum(case when severity = 'unknown' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_unknown,
				      sum(case when severity = 'unknown' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_unknown,
				      sum(case when severity = 'unknown' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_unknown,
				      sum(case when severity = 'medium' then 1 else 0 end) medium,
				      sum(case when severity = 'medium' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_medium,
				      sum(case when severity = 'medium' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_medium,
				      sum(case when severity = 'medium' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_medium,
				      sum(case when severity = 'low' then 1 else 0 end) low,
				      sum(case when severity = 'low' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_low,
				      sum(case when severity = 'low' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_low,
				      sum(case when severity = 'low' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_low,
				      sum(case when severity = 'info' then 1 else 0 end) info,
				      sum(case when severity = 'info' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_info,
				      sum(case when severity = 'info' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_info,
				      sum(case when severity = 'info' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_info,
				      sum(1) total
				    from vulnerabilities
				    where
				    (state = 'dismissed' and dismissed_at >= datetime('now', '-6 months'))
				    or (state = 'confirmed' and updated_at >= datetime('now', '-6 months'))
				    or (state = 'resolved' and resolved_at >= datetime('now', '-6 months'))
				    group by 1
				    order by 2
				    )
				    select
				      strftime('%W', dates.reportWeek) WeekNumber,
				      date(dates.reportWeek, 'weekday 0', '-6 day') WeekStart,
				      date(dates.reportWeek, 'weekday 0') WeekEnd,
				      coalesce(critical,0) critical,
				      coalesce(dismissed_critical,0) dismissed_critical,
				      coalesce(confirmed_critical,0) confirmed_critical,
				      coalesce(resolved_critical,0) resolved_critical,
				      coalesce(high,0) high,
				      coalesce(dismissed_high,0) dismissed_high,
				      coalesce(confirmed_high,0) confirmed_high,
				      coalesce(resolved_high,0) resolved_high,
				      coalesce(unknown,0) unknown,
				      coalesce(dismissed_unknown,0) dismissed_unknown,
				      coalesce(confirmed_unknown,0) confirmed_unknown,
				      coalesce(resolved_unknown,0) resolved_unknown,
				      coalesce(medium,0) medium,
				      coalesce(dismissed_medium,0) dismissed_medium,
				      coalesce(confirmed_medium,0) confirmed_medium,
				      coalesce(resolved_medium,0) resolved_medium,
				      coalesce(low,0) low,
				      coalesce(dismissed_low,0) dismissed_low,
				      coalesce(confirmed_low,0) confirmed_low,
				      coalesce(resolved_low,0) resolved_low,
				      coalesce(info,0) info,
				      coalesce(dismissed_info,0) dismissed_info,
				      coalesce(confirmed_info,0) confirmed_info,
				      coalesce(resolved_info,0) resolved_info,
				      coalesce(total,0) total
				    from dates left outer join weekly on  strftime('%W', dates.reportWeek) = weekly.WeekNumber
			END
    )
    sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/velocity_by_week.json

    request=$(
      cat <<-END
				    with dates as (
				      select datetime('now', '-6 months') as month
				      union all
				      select datetime(month, '+1 month')
				      from dates
				      where month < datetime('now', 'weekday 0', '-7 days')
				     ), monthly as (
				    select strftime('%m', case vulnerabilities.state when 'dismissed' then dismissed_at when 'confirmed' then updated_at when 'resolved' then resolved_at end) month,
				      sum(case when severity = 'critical' then 1 else 0 end) critical,
				      sum(case when severity = 'critical' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_critical,
				      sum(case when severity = 'critical' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_critical,
				      sum(case when severity = 'critical' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_critical,
				      sum(case when severity = 'high' then 1 else 0 end) high,
				      sum(case when severity = 'high' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_high,
				      sum(case when severity = 'high' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_high,
				      sum(case when severity = 'high' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_high,
				      sum(case when severity = 'unknown' then 1 else 0 end) unknown,
				      sum(case when severity = 'unknown' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_unknown,
				      sum(case when severity = 'unknown' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_unknown,
				      sum(case when severity = 'unknown' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_unknown,
				      sum(case when severity = 'medium' then 1 else 0 end) medium,
				      sum(case when severity = 'medium' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_medium,
				      sum(case when severity = 'medium' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_medium,
				      sum(case when severity = 'medium' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_medium,
				      sum(case when severity = 'low' then 1 else 0 end) low,
				      sum(case when severity = 'low' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_low,
				      sum(case when severity = 'low' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_low,
				      sum(case when severity = 'low' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_low,
				      sum(case when severity = 'info' then 1 else 0 end) info,
				      sum(case when severity = 'info' and vulnerabilities.state = 'dismissed' then 1 else 0 end) dismissed_info,
				      sum(case when severity = 'info' and vulnerabilities.state = 'confirmed' then 1 else 0 end) confirmed_info,
				      sum(case when severity = 'info' and vulnerabilities.state = 'resolved' then 1 else 0 end) resolved_info,
				      sum(1) total
				    from vulnerabilities
				    where
				      (state = 'dismissed' and dismissed_at >= datetime('now', '-6 months'))
				      or (state = 'confirmed' and updated_at >= datetime('now', '-6 months'))
				      or (state = 'resolved' and resolved_at >= datetime('now', '-6 months'))
				    group by 1
				    order by 2
				    )
				    select
				      strftime('%Y-%m', dates.month) month,
				      coalesce(critical,0) critical,
				      coalesce(dismissed_critical,0) dismissed_critical,
				      coalesce(confirmed_critical,0) confirmed_critical,
				      coalesce(resolved_critical,0) resolved_critical,
				      coalesce(high,0) high,
				      coalesce(dismissed_high,0) dismissed_high,
				      coalesce(confirmed_high,0) confirmed_high,
				      coalesce(resolved_high,0) resolved_high,
				      coalesce(unknown,0) unknown,
				      coalesce(dismissed_unknown,0) dismissed_unknown,
				      coalesce(confirmed_unknown,0) confirmed_unknown,
				      coalesce(resolved_unknown,0) resolved_unknown,
				      coalesce(medium,0) medium,
				      coalesce(dismissed_medium,0) dismissed_medium,
				      coalesce(confirmed_medium,0) confirmed_medium,
				      coalesce(resolved_medium,0) resolved_medium,
				      coalesce(low,0) low,
				      coalesce(dismissed_low,0) dismissed_low,
				      coalesce(confirmed_low,0) confirmed_low,
				      coalesce(resolved_low,0) resolved_low,
				      coalesce(info,0) info,
				      coalesce(dismissed_info,0) dismissed_info,
				      coalesce(confirmed_info,0) confirmed_info,
				      coalesce(resolved_info,0) resolved_info,
				      coalesce(total,0) total
				    from dates left outer join monthly on  strftime('%m', dates.month) = monthly.month
			END
    )
    sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/velocity_by_month.json

    # Breakdown (Open vulnerabilities)
    request=$(
      cat <<-END
				    select projects.path_with_namespace, sum(case when vulnerabilities.severity in ('critical', 'high', 'unknown') then 1 else 0 end) critical_high_unknown, sum(case when vulnerabilities.severity not in ('critical', 'high', 'unknown') then 1 else 0 end) others, sum(1) total
				    from vulnerabilities
				      join projects on vulnerabilities.project_id = projects.id
				    where vulnerabilities.state in ('detected','confirmed')
				    group by 1
				    order by total DESC
			END
    )
    sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/breakdown.json

    # Breakdown (False postives)
    request=$(
      cat <<-END
				    select projects.path_with_namespace, sum(case when severity in ('critical', 'high', 'unknown') then 1 else 0 end) critical_high_unknown, sum(case when severity not in ('critical', 'high', 'unknown') then 1 else 0 end) others, sum(1) total
				    from vulnerabilities
				      join projects on vulnerabilities.project_id = projects.id
				    where vulnerabilities.state in ('dismissed')
				    group by 1
				    order by total DESC
			END
    )
    sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/breakdown_fp.json
  fi
}

# ==============================

echo "Generating pages..."
generate_projects_pages
generate_docker_images_requested_pages

echo "Generating projects metric..."
generate_projects_metrics

# Generate site
hugo -s ${INVENTORY_SITE} -d ../../public

if [ -n "${HUGO_BASEURL:-}" ]; then
  echo "Your inventory reports are ready and will be available at: $HUGO_BASEURL"
fi
