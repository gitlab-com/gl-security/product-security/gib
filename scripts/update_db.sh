#!/bin/bash
# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

declare -xr DATADIR=${DATA_DIR_PATH:=./data}
declare -xr INVENTORY_DB=./db/inventory.db
declare -xr SSL_REPORTS_PATH=${SSL_REPORTS_PATH:=tmp/ssl}

# Project files
declare -xr DEPENDENCIES="dependencies.json"
declare -xr VULNERABILITIES="vulnerabilities.json"
declare -xr CI_CONFIG="ci-config.json"
declare -xr PROJECTS_VIOLATIONS="violations.json"
declare -xr PROTECTED_BRANCHES="protected_branches.json"
declare -xr APPROVALS="approvals.json"
declare -xr APPROVAL_RULES="approval_rules.json"
declare -xr CICD_SETTINGS="cicd_settings.json"
declare -xr IMAGES_REQUESTED="images-requested.json"
declare -xr JOB_TOKEN_SCOPE_ALLOWLIST="job_token_scope_allowlist.json"

# Early exit if DATADIR is empty (noop)
if [ "$(find "${DATADIR}" -maxdepth 0 -empty | wc -c)" -gt 0 ]; then
  echo "${DATADIR} is empty."
  exit 0
fi

# init
mkdir -p $DATADIR/../db
echo -n "Using database: $(realpath $INVENTORY_DB)"
# Reset DB
true >$INVENTORY_DB

# variables for foreign keys are set when at least one record, so the table is created
declare -x CATEGORIES_FK=""
declare -x URLS_FK=""
declare -x DEPENDENCIES_FK=""
declare -x VULNERABILITIES_FK=""
declare -x CI_CONFIG_FK=""
declare -x PROJECTS_VIOLATIONS_FK=""
declare -x PROJECTS_PROTECTED_BRANCHES_FK=""
declare -x PROJECTS_APPROVALS_FK=""
declare -x PROJECTS_APPROVAL_RULES_FK=""
declare -x PROJECTS_CICD_SETTINGS_FK=""
declare -x URL_VIOLATIONS_FK=""
declare -x IMAGES_REQUESTED_FK=""
declare -x JOB_TOKEN_SCOPE_ALLOWLIST_FK=""

# Hide progress bar when inserting
function sqlite-insert() {
  sqlite-utils insert --silent "$@"
}

# store projects
find "$DATADIR" -type f -name project.json -print0 |
  while IFS= read -r -d '' project_json; do
    PROJECT=$(dirname "$project_json")
    echo -ne "\n[$PROJECT] Storing: "
    echo -n "Project"
    sqlite-insert $INVENTORY_DB projects "$project_json" --pk=id --replace --alter

    if [[ -f "$PROJECT/properties.yml" ]]; then
      # Store categories
      echo -n ", Categories"
      yq e '.categories' -o=json "$PROJECT/properties.yml" | jq --argjson projectMeta "$(<"$project_json")" '[.] | transpose | map({ category:.[0], project_id: $projectMeta.id })' | sqlite-insert $INVENTORY_DB categories --pk=category --pk=project_id --replace --alter -
      CATEGORIES_FK="categories project_id projects id"

      # Store URLs
      if [[ "$(yq e '.urls | length' "$PROJECT/properties.yml")" -gt "0" ]]; then
        echo -n ", URLs"
        yq e '.urls' -o=json "$PROJECT/properties.yml" | jq --argjson projectMeta "$(<"$project_json")" '[.] | transpose | map({ url:.[0], project_id: $projectMeta.id })' | sqlite-insert $INVENTORY_DB urls --pk=url --pk=project_id --replace --alter -
        URLS_FK="urls project_id projects id"
      fi
    fi

    if [[ -f "$PROJECT/$DEPENDENCIES" ]]; then
      echo -n ", Licences"
      # Store licenses (currently [name, url])
      jq '[.[].licenses[]?] | unique' "$PROJECT/$DEPENDENCIES" | sqlite-insert $INVENTORY_DB licenses --pk=name --replace --alter -
      echo -n ", Dependencies"
      # Add project_id to all entries, and flatten licenses to keep only an array of their names
      jq --argjson projectMeta "$(<"$project_json")" '.[].project_id += $projectMeta.id | .[].licenses |= (. // [] | map(.name))' "$PROJECT/$DEPENDENCIES" | sqlite-insert $INVENTORY_DB dependencies --pk=name --pk=version --pk=dependency_file_path --pk=project_id --replace --alter -
      DEPENDENCIES_FK="dependencies project_id projects id"
    fi

    if [[ -f "$PROJECT/$VULNERABILITIES" ]]; then
      echo -n ", Vulnerabilities"
      sqlite-insert $INVENTORY_DB vulnerabilities --pk=id --replace --alter --flatten $PROJECT/$VULNERABILITIES
      VULNERABILITIES_FK="vulnerabilities project_id projects id"
    fi

    if [[ -f "$PROJECT/$CI_CONFIG" ]]; then
      echo -n ", CI-configuration"
      jq --argjson projectMeta "$(<"$project_json")" '.project_id += $projectMeta.id' "$PROJECT/$CI_CONFIG" | sqlite-insert $INVENTORY_DB ci_configs --pk=project_id --replace --alter -
      CI_CONFIG_FK="ci_configs project_id projects id"
    fi

    if [[ -f "$PROJECT/$PROJECTS_VIOLATIONS" ]] && [[ "$(<"$PROJECT/$PROJECTS_VIOLATIONS")" != "{}" ]]; then
      echo -n ", Violations"
      jq --argjson projectMeta "$(<"$project_json")" '[.result[].bindings.violation] | map({ violation:.msg, description:.description, key:.key, project_id: $projectMeta.id })' "$PROJECT/$PROJECTS_VIOLATIONS" | sqlite-insert $INVENTORY_DB projects_violations --pk=violation --pk=key --pk=project_id --replace --alter -
      PROJECTS_VIOLATIONS_FK="projects_violations project_id projects id"
    fi

    if [[ -f "$PROJECT/$PROTECTED_BRANCHES" ]] && [[ "$(<"$PROJECT/$PROTECTED_BRANCHES")" != "{}" ]]; then
      echo -n ", Protected branches"
      jq --argjson projectMeta "$(<"$project_json")" '.[].project_id += $projectMeta.id' "$PROJECT/$PROTECTED_BRANCHES" | sqlite-insert $INVENTORY_DB protected_branches --pk=id --replace --alter -
      PROJECTS_PROTECTED_BRANCHES_FK="protected_branches project_id projects id"
    fi

    if [[ -f "$PROJECT/$APPROVALS" ]] && [[ "$(<"$PROJECT/$APPROVALS")" != "{}" ]]; then
      echo -n ", Approvals"
      jq --argjson projectMeta "$(<"$project_json")" '.project_id += $projectMeta.id' "$PROJECT/$APPROVALS" | sqlite-insert $INVENTORY_DB approvals --pk=id --replace --alter -
      PROJECTS_APPROVALS_FK="approvals project_id projects id"
    fi

    if [[ -f "$PROJECT/$APPROVAL_RULES" ]] && [[ "$(<"$PROJECT/$APPROVAL_RULES")" != "{}" ]]; then
      echo -n ", Approvals Rules"
      jq --argjson projectMeta "$(<"$project_json")" '.[].project_id += $projectMeta.id' "$PROJECT/$APPROVAL_RULES" | sqlite-insert $INVENTORY_DB approval_rules --pk=id --replace --alter -
      PROJECTS_APPROVAL_RULES_FK="approvals_rules project_id projects id"
    fi

    if [[ -f "$PROJECT/$CICD_SETTINGS" ]] && [[ "$(<"$PROJECT/$CICD_SETTINGS")" != "{}" ]]; then
      echo -n ", CI/CD Settings"
      jq --argjson projectMeta "$(<"$project_json")" '.project_id += $projectMeta.id' "$PROJECT/$CICD_SETTINGS" | sqlite-insert $INVENTORY_DB cicd_settings --pk=id --replace --alter -
      PROJECTS_CICD_SETTINGS_FK="cicd_settings project_id projects id"
    fi

    if [[ -f "$PROJECT/$IMAGES_REQUESTED" ]] && [[ "$(jq '.images | length' "$PROJECT/$IMAGES_REQUESTED")" != "0" ]]; then
      echo -n ", Images requested"
      jq --argjson projectMeta "$(<"$project_json")" '.images | to_entries | map(.key as $key | .value[] | {"image_name": $key} + . + {"project_id": $projectMeta.id})' "$PROJECT/$IMAGES_REQUESTED" | sqlite-insert $INVENTORY_DB images_requested --pk=id --replace --alter -
      IMAGES_REQUESTED_FK="images_requested project_id projects id"
    fi

    if [[ -f "$PROJECT/$JOB_TOKEN_SCOPE_ALLOWLIST" ]] && [[ "$(jq 'length' "$PROJECT/$JOB_TOKEN_SCOPE_ALLOWLIST")" != "0" ]]; then
      echo -n ", JobTokenInboundAllowList"
      jq --argjson projectMeta "$(<"$project_json")" '[.[] | {"project_id": $projectMeta.id, "allowed_project_id": .id}]' "$PROJECT/$JOB_TOKEN_SCOPE_ALLOWLIST" | sqlite-insert $INVENTORY_DB job_token_scope_allowlists --pk=project_id --pk=allowed_project_id --replace --alter -
      JOB_TOKEN_SCOPE_ALLOWLIST_FK="job_token_scope_allowlists project_id projects id"
    fi
  done

# store groups
find "$DATADIR" -type f -name group.json -print0 |
  while IFS= read -r -d '' group_json; do
    GROUP=$(dirname "$group_json")
    echo -ne "\n[$GROUP] Storing: "
    echo -n "Group"
    sqlite-insert $INVENTORY_DB groups "$group_json" --pk=id --replace --alter
  done

if [ -d "$SSL_REPORTS_PATH" ]; then
  # store URLs violations
  find $SSL_REPORTS_PATH -type f -name '*.violations.json' -print0 |
    while IFS= read -r -d '' violations_json; do
      if [[ "$(<"$violations_json")" != "{}" ]]; then
        URL=$(echo "https://$(basename $violations_json .violations.json)")
        echo -ne "\n[$URL] Storing: "
        echo -n "URL violation"
        jq --arg url "$URL" '[.result[].bindings.violation] | map({ violation:.msg, description:.description, key:.key, url: $url })' $violations_json | sqlite-insert $INVENTORY_DB urls_violations --pk=url --pk=key --replace --alter -
        URL_VIOLATIONS_FK="urls_violations url urls url"
      fi
    done
fi

# Ensure every foreign key has a corresponding index
sqlite-utils add-foreign-keys $INVENTORY_DB \
  $CATEGORIES_FK \
  $URLS_FK \
  $DEPENDENCIES_FK \
  $VULNERABILITIES_FK \
  $CI_CONFIG_FK \
  $PROJECTS_VIOLATIONS_FK \
  $PROJECTS_PROTECTED_BRANCHES_FK \
  $PROJECTS_APPROVALS_FK \
  $PROJECTS_APPROVAL_RULES_FK \
  $PROJECTS_CICD_SETTINGS_FK \
  $IMAGES_REQUESTED_FK \
  $JOB_TOKEN_SCOPE_ALLOWLIST_FK \
  $URL_VIOLATIONS_FK
sqlite-utils index-foreign-keys $INVENTORY_DB

# Display table stats
echo -e "\n"
sqlite-utils tables $INVENTORY_DB --counts -t

# Create a view with all projects categorized as "product"
echo -e "\n"
echo "Create 'product_projects' view..."
sqlite-utils create-view "$INVENTORY_DB" product_projects "$(
  cat <<EOF
  SELECT projects.id, projects.path_with_namespace, group_concat(categories.category) as project_categories
  FROM projects
  JOIN categories ON projects.id = categories.project_id
  GROUP BY projects.id, projects.path_with_namespace
  HAVING project_categories LIKE '%product%';
EOF
)"

# Compress the database with 7zip
echo "Compress DB..."
7z a $INVENTORY_DB.7z db/inventory.db
