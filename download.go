package main

import (
	"reflect"

	log "github.com/sirupsen/logrus"
	gitlab "gitlab.com/gitlab-org/api/client-go"
	"golang.org/x/sync/errgroup"
)

// DownloadResponse represents a generic response type from GitLab API
type DownloadResponse[T any] struct {
	Items    []*T
	Response *gitlab.Response
	Error    error
}

// DownloadFunction represents a generic download function that returns items of type T
type DownloadFunction[T any] func(page int) ([]*T, *gitlab.Response, error)

// LoggerFunc represents a function that returns a logger
type LoggerFunc func() *log.Entry

func downloadPaginatedItems[T any](
	downloader DownloadFunction[T],
	logger LoggerFunc,
	ch chan interface{},
) error {
	// Get the type name for logging
	var zero T
	typeName := reflect.TypeOf(zero).Name()
	if typeName == "" {
		typeName = "items" // fallback if type name cannot be determined
	}

	// The number of pages isn't known in advance, at least one request must be done to get this value
	items, response, err := downloader(1)
	if err != nil {
		return err
	}
	ch <- items

	pages := response.TotalPages
	nextPage := 0
	// If a query returns more than 10,000 records, x-total-pages isn't returned and defaults to 0
	if pages == 0 {
		pages = 99
		nextPage = 100 // We need the last page response to get the next page (if exists)
	}

	// Download pages asynchronously
	g := new(errgroup.Group)
	for page := 2; page < pages+1; page++ {
		page := page // https://go.dev/doc/faq#closures_and_goroutines
		g.Go(func() error {
			if page%10 == 0 {
				logger().Debugf("Downloading %s page %d", typeName, page)
			}
			items, _, err := downloader(page)
			if err != nil {
				return err
			}
			ch <- items
			return nil
		})
	}

	// Download remaining pages
	for nextPage != 0 {
		if nextPage%10 == 0 {
			logger().Debugf("Downloading %s page %d", typeName, nextPage)
		}
		items, response, err := downloader(nextPage)
		if err != nil {
			return err
		}
		ch <- items
		nextPage = response.NextPage
	}

	// Wait for the concurrent requests to finish
	return g.Wait()
}
